#include "app_sdl.h"
#include <fstream>
#include "core_log.h"
#include <tiny_gltf.h>
#include <glm/gtc/matrix_transform.hpp>
#include <thread>

static App *app;

/**************************************************************************************************/
App::App() : VulkanSdl( "SDL" )
{
}

/**************************************************************************************************/
App::~App()
{
}

/**************************************************************************************************/
void App::prepare()
{
}

/**************************************************************************************************/
void App::unprepare()
{
}

/**************************************************************************************************/
void App::fillCommandBufferPreRenderPass()
{
}

/**************************************************************************************************/
void App::fillCommandBuffer()
{
}


/**************************************************************************************************/
int main( int, char *[] )
{
    app = new App();
    app->create();

    int i = 0;

    while( app->m_window.pollWindow() )
    {
        if( app->acquireNextFrame() )
        {
            app->renderNextFrame();
            app->presentNextFrame();
        }
    }

    app->destroy();
    delete( app );
    app = nullptr;

    std::cout << "Done" << std::endl;
    return 0;
}
