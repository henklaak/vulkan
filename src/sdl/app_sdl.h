#pragma once
#include "sdl_basis.h"

class App: public VulkanSdl
{
public:
    struct UBOVS
    {
        glm::mat4 projection = glm::mat4( 1 );
        glm::mat4 model = glm::mat4( 1 );
        glm::mat4 matrix = glm::mat4( 1 );
    } ;

    App();
    virtual ~App();

    void prepare() override;
    void unprepare() override;

    void fillCommandBufferPreRenderPass() override;
    void fillCommandBuffer() override;

    void doit();

    void prepareRenderPipeline();

    VkPipeline m_pipelineRender = VK_NULL_HANDLE;
    VkPipelineLayout m_pipelineLayoutRender = VK_NULL_HANDLE;
    VkDescriptorSetLayout m_descriptorSetLayout = VK_NULL_HANDLE;
    VkDescriptorSet m_descriptorSet = VK_NULL_HANDLE;
};
