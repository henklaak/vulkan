#pragma once
#include "sdl_basis.h"

class Camera;

class App: public VulkanSdl
{
public:
    App();
    virtual ~App();

    void prepare( ) override;
    void prepareRenderImagesSamplers();
    void prepareRenderPipeline();
    void prepareRenderDescriptorSets();
    void updateRenderDescriptors();

    void prepareComputePipeline();
    void prepareComputeDescriptorSets();
    void updateComputeDescriptors();

    void unprepare() override;

    void grab();
    void grabCB( void *dataptr,
                 uint32_t width,
                 uint32_t height );
    void refreshTextureImage();

    void fillCommandBufferPreRenderPass() override;
    void fillCommandBuffer() override;
    //VkShaderModule createShaderModule( const std::vector<char> &code );

    Camera *m_cam = nullptr;

    VkBuffer m_bufferStage = VK_NULL_HANDLE;;
    VkDeviceMemory m_memoryStage = VK_NULL_HANDLE;

    VkImage m_imageTexture = VK_NULL_HANDLE;
    VkDeviceMemory m_memoryTexture = VK_NULL_HANDLE;
    VkImageView m_imageViewTexture = VK_NULL_HANDLE;
    VkSampler m_samplerTexture = VK_NULL_HANDLE;

    VkImage m_imageOut = VK_NULL_HANDLE;
    VkDeviceMemory m_memoryOut = VK_NULL_HANDLE;
    VkImageView m_imageViewOut = VK_NULL_HANDLE;


    VkPipeline m_pipelineRender = VK_NULL_HANDLE;
    VkPipelineLayout m_pipelineLayoutRender = VK_NULL_HANDLE;
    VkDescriptorSetLayout m_descriptorSetLayoutRender = VK_NULL_HANDLE;

    VkDescriptorPool m_descriptorPoolRender = VK_NULL_HANDLE;
    std::array<VkDescriptorSet, MAX_FRAMES_IN_FLIGHT> m_descriptorSetsRender;
    VkSamplerYcbcrConversion m_conversion = VK_NULL_HANDLE;

    VkPipeline m_pipelineCompute = VK_NULL_HANDLE;
    VkPipelineLayout m_pipelineLayoutCompute = VK_NULL_HANDLE;
    VkDescriptorSetLayout m_descriptorSetLayoutCompute = VK_NULL_HANDLE;

    VkDescriptorPool m_descriptorPoolCompute = VK_NULL_HANDLE;
    std::array<VkDescriptorSet, MAX_FRAMES_IN_FLIGHT> m_descriptorSetsCompute;

    std::array<VkBuffer, MAX_FRAMES_IN_FLIGHT> m_buffersResult;
    std::array<VkDeviceMemory, MAX_FRAMES_IN_FLIGHT> m_memoriesResult;
};
