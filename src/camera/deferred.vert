#version 450

vec2 positions[4] = vec2[](
    vec2(-1, -1),
    vec2(-1,  1),
    vec2( 1, -1),
    vec2( 1,  1)
);

vec2 fragTexCoords[4] = vec2[](
    vec2(0, 0),
    vec2(0, 1),
    vec2(1, 0),
    vec2(1, 1)
);

layout(location = 0) out vec2 fragTexCoord;

void main()
{
    gl_Position = vec4(positions[gl_VertexIndex], 0.5, 1.0);
    fragTexCoord = fragTexCoords[gl_VertexIndex];
}
