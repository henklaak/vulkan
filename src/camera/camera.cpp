#include "camera.h"
#include <cassert>
#include <iostream>
#include <stdexcept>

#include <fcntl.h>
#include <poll.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include <stb/stb_image_write.h>

const int VERBOSE = 0;

/**************************************************************************************************/
static int xioctl( int fd, int request, void *arg )
{
    int r;

    do
    {
        r = ioctl( fd, request, arg );
    }
    while( -1 == r && EINTR == errno );

    return r;
}

/**************************************************************************************************/
Camera::Camera( const std::string &device )
    : m_device( device )
{
    openDevice();
    getControls();
    getFormats();
    getFrameSizes();
    getFrameIntervals();
    setFormat();
    setupBuffers();
    startStreaming();
}

/**************************************************************************************************/
Camera::~Camera()
{
    stopStreaming();
    destroyBuffers();
    closeDevice();
}

/**************************************************************************************************/
void Camera::setBrightnessDelta( int delta )
{
    struct v4l2_queryctrl queryctrl {};
    queryctrl.id = V4L2_CID_BRIGHTNESS;

    if( xioctl( m_fd, VIDIOC_QUERYCTRL, &queryctrl ) )
    {
        throw std::runtime_error( "VIDIOC_QUERYCTRL" );
    }

    struct v4l2_control control {};

    control.id = V4L2_CID_BRIGHTNESS;

    if( xioctl( m_fd, VIDIOC_G_CTRL, &control ) )
    {
        throw std::runtime_error( "VIDIOC_G_CTRL" );
    }

    if( delta == 0 )
    {
        setAutoExposure( true );
        control.value = queryctrl.default_value;
    }
    else
    {
        setAutoExposure( false );
        control.value = std::max( queryctrl.minimum, std::min( control.value + delta, queryctrl.maximum ) );
    }

    if( ( queryctrl.flags & V4L2_CTRL_FLAG_DISABLED ) ||
            ( queryctrl.flags & V4L2_CTRL_FLAG_INACTIVE ) )
    {
        return;
    }

    if( xioctl( m_fd, VIDIOC_S_CTRL, &control ) )
    {
        throw std::runtime_error( "VIDIOC_S_CTRL" );
    }
}

/**************************************************************************************************/
void Camera::setContrastDelta( int delta )
{
    struct v4l2_queryctrl queryctrl {};
    queryctrl.id = V4L2_CID_CONTRAST;

    if( xioctl( m_fd, VIDIOC_QUERYCTRL, &queryctrl ) )
    {
        throw std::runtime_error( "VIDIOC_QUERYCTRL" );
    }

    struct v4l2_control control {};

    control.id = V4L2_CID_CONTRAST;

    if( xioctl( m_fd, VIDIOC_G_CTRL, &control ) )
    {
        throw std::runtime_error( "VIDIOC_G_CTRL" );
    }

    if( delta == 0 )
    {
        setAutoExposure( true );
        control.value = queryctrl.default_value;
    }
    else
    {
        setAutoExposure( false );
        control.value = std::max( queryctrl.minimum, std::min( control.value + delta, queryctrl.maximum ) );
    }

    if( ( queryctrl.flags & V4L2_CTRL_FLAG_DISABLED ) ||
            ( queryctrl.flags & V4L2_CTRL_FLAG_INACTIVE ) )
    {
        return;
    }

    if( xioctl( m_fd, VIDIOC_S_CTRL, &control ) )
    {
        throw std::runtime_error( "VIDIOC_S_CTRL" );
    }
}

/**************************************************************************************************/
void Camera::setGainDelta( int delta )
{
    struct v4l2_queryctrl queryctrl {};
    queryctrl.id = V4L2_CID_GAIN;

    if( xioctl( m_fd, VIDIOC_QUERYCTRL, &queryctrl ) )
    {
        throw std::runtime_error( "VIDIOC_QUERYCTRL" );
    }

    struct v4l2_control control {};

    control.id = V4L2_CID_GAIN;

    if( xioctl( m_fd, VIDIOC_G_CTRL, &control ) )
    {
        throw std::runtime_error( "VIDIOC_G_CTRL" );
    }

    if( delta == 0 )
    {
        setAutoExposure( true );
        control.value = queryctrl.default_value;
    }
    else
    {
        setAutoExposure( false );
        control.value = std::max( queryctrl.minimum, std::min( control.value + delta, queryctrl.maximum ) );
    }

    if( ( queryctrl.flags & V4L2_CTRL_FLAG_DISABLED ) ||
            ( queryctrl.flags & V4L2_CTRL_FLAG_INACTIVE ) )
    {
        return;
    }


    if( xioctl( m_fd, VIDIOC_S_CTRL, &control ) )
    {
        throw std::runtime_error( "VIDIOC_S_CTRL" );
    }
}


/**************************************************************************************************/
void Camera::setExposureDelta( int delta )
{
    struct v4l2_queryctrl queryctrl {};
    queryctrl.id = V4L2_CID_EXPOSURE_ABSOLUTE;

    struct v4l2_control control {};
    control.id = V4L2_CID_EXPOSURE_ABSOLUTE;

    if( delta == 0 )
    {
        setAutoExposure( true );

        if( xioctl( m_fd, VIDIOC_QUERYCTRL, &queryctrl ) )
        {
            throw std::runtime_error( "VIDIOC_QUERYCTRL" );
        }

        control.value = queryctrl.default_value;
    }
    else
    {
        setAutoExposure( false );

        if( xioctl( m_fd, VIDIOC_G_CTRL, &control ) )
        {
            throw std::runtime_error( "VIDIOC_G_CTRL" );
        }

        if( xioctl( m_fd, VIDIOC_QUERYCTRL, &queryctrl ) )
        {
            throw std::runtime_error( "VIDIOC_QUERYCTRL" );
        }

        control.value = std::max( queryctrl.minimum, std::min( control.value + delta, queryctrl.maximum ) );
    }

    if( ( queryctrl.flags & V4L2_CTRL_FLAG_DISABLED ) ||
            ( queryctrl.flags & V4L2_CTRL_FLAG_INACTIVE ) )
    {
        return;
    }

    control.value = delta;

    if( xioctl( m_fd, VIDIOC_S_CTRL, &control ) )
    {
        throw std::runtime_error( "VIDIOC_S_CTRL" );
    }
}

/**************************************************************************************************/
void Camera::setAutoExposure( bool enable )
{
    struct v4l2_control control {};

    control.id = V4L2_CID_EXPOSURE_AUTO;
    control.value = enable ? 3 : 1;

    if( xioctl( m_fd, VIDIOC_S_CTRL, &control ) )
    {
        throw std::runtime_error( "VIDIOC_S_CTRL" );
    }

    control.id = V4L2_CID_EXPOSURE_AUTO_PRIORITY;
    control.value = enable ? 1 : 0;

    if( xioctl( m_fd, VIDIOC_S_CTRL, &control ) )
    {
        throw std::runtime_error( "VIDIOC_S_CTRL" );
    }
}

/**************************************************************************************************/
uint32_t Camera::width()
{
    return m_frameSizes[m_frameSizeIndex].discrete.width;
}

/**************************************************************************************************/
uint32_t Camera::height()
{
    return m_frameSizes[m_frameSizeIndex].discrete.height;
}

/**************************************************************************************************/
void Camera::openDevice()
{
    m_fd = open( m_device.c_str(), O_RDWR );
    assert( m_fd >= 0 );
}

/**************************************************************************************************/
void Camera::closeDevice()
{
    close( m_fd );
    m_fd = -1;
}

/**************************************************************************************************/
void Camera::getControls()
{
    const unsigned next_fl = V4L2_CTRL_FLAG_NEXT_CTRL | V4L2_CTRL_FLAG_NEXT_COMPOUND;
    struct v4l2_queryctrl qctrl {};
    qctrl.id = next_fl;

    while( 0 == xioctl( m_fd, VIDIOC_QUERYCTRL, &qctrl ) )
    {
        qctrl.id |= next_fl;
    }

    setBrightnessDelta( 0 );
    setContrastDelta( 0 );
    setGainDelta( 0 );
    setExposureDelta( 0 );
}

/**************************************************************************************************/
void Camera::getFormats()
{
    m_formats.clear();

    static struct v4l2_fmtdesc fmtdesc
    {
        .index = 0,
        .type = V4L2_BUF_TYPE_VIDEO_CAPTURE,
    };

    for( ;; )
    {
        if( xioctl( m_fd, VIDIOC_ENUM_FMT, &fmtdesc ) )
        {
            break;
        }

        if( VERBOSE >= 2 )
        {
            std::cout << fmtdesc.index << ": "
                      << ( char * )( fmtdesc.description ) << std::endl;
        }

        m_formats.push_back( fmtdesc );
        fmtdesc.index++;
    }

    m_formatIndex = 1;

    if( VERBOSE >= 2 )
    {
        std::cout << m_formatIndex << std::endl;
    }
}

/**************************************************************************************************/
void Camera::getFrameSizes()
{
    m_frameSizes.clear();

    static struct v4l2_frmsizeenum frmsizeenum
    {
        .index = 0,
        .pixel_format = m_formats[m_formatIndex].pixelformat,
    };

    for( ;; )
    {
        if( xioctl( m_fd, VIDIOC_ENUM_FRAMESIZES, &frmsizeenum ) )
        {
            break;
        }

        if( VERBOSE >= 2 )
        {
            std::cout << frmsizeenum.index << ": "
                      << frmsizeenum.discrete.width << " x "
                      << frmsizeenum.discrete.height << std::endl;
        }

        m_frameSizes.push_back( frmsizeenum );
        frmsizeenum.index++;
    }

    m_frameSizeIndex = m_frameSizes.size() - 1;

    if( VERBOSE >= 2 )
    {
        std::cout << m_frameSizeIndex << std::endl;
    }
}

/**************************************************************************************************/
void Camera::getFrameIntervals()
{
    m_frameIntervals.clear();

    struct v4l2_frmivalenum frmivalenum
    {
        .index = 0,
        .pixel_format = m_formats[m_formatIndex].pixelformat,
        .width = m_frameSizes[m_frameSizeIndex].discrete.width,
        .height = m_frameSizes[m_frameSizeIndex].discrete.height,
    };

    for( ;; )
    {
        if( xioctl( m_fd, VIDIOC_ENUM_FRAMEINTERVALS, &frmivalenum ) )
        {
            break;;
        }

        if( VERBOSE >= 2 )
        {
            std::cout << frmivalenum.index << ": "
                      << frmivalenum.discrete.numerator << " / "
                      << frmivalenum.discrete.denominator << std::endl;
        }


        m_frameIntervals.push_back( frmivalenum );
        frmivalenum.index++;
    }

    m_frameIntervalIndex = 0;

    if( VERBOSE >= 2 )
    {
        std::cout << m_frameIntervalIndex << std::endl;
    }
}

/**************************************************************************************************/
void Camera::setFormat()
{
    v4l2_format m_fmt
    {
        .type = V4L2_BUF_TYPE_VIDEO_CAPTURE,
        .fmt{
            .pix{
                .width = m_frameSizes[m_frameSizeIndex].discrete.width,
                .height = m_frameSizes[m_frameSizeIndex].discrete.height,
                .pixelformat = m_formats[m_formatIndex].pixelformat,
                .field = V4L2_FIELD_NONE,
            },
        },
    };

    if( xioctl( m_fd, VIDIOC_S_FMT, &m_fmt ) )
    {
        throw std::runtime_error( "VIDIOC_S_FMT" );
    }

    v4l2_streamparm streamparm{.type = V4L2_BUF_TYPE_VIDEO_CAPTURE};

    if( xioctl( m_fd, VIDIOC_G_PARM, &streamparm ) )
    {
        throw std::runtime_error( "VIDIOC_G_PARM" );
    }

    streamparm.parm.capture.timeperframe.numerator = m_frameIntervals[m_frameIntervalIndex].discrete.numerator;
    streamparm.parm.capture.timeperframe.denominator = m_frameIntervals[m_frameIntervalIndex].discrete.denominator;

    if( xioctl( m_fd, VIDIOC_S_PARM, &streamparm ) )
    {
        throw std::runtime_error( "VIDIOC_S_PARM" );
    }
}

/**************************************************************************************************/
void Camera::setupBuffers()
{
    struct v4l2_requestbuffers req
    {
        .count = 3,
        .type = V4L2_BUF_TYPE_VIDEO_CAPTURE,
        .memory = V4L2_MEMORY_MMAP
    };

    if( xioctl( m_fd, VIDIOC_REQBUFS, &req ) )
    {
        throw std::runtime_error( "VIDIOC_REQBUFS" );
    }

    m_bufferDescriptors.resize( req.count );

    for( size_t i = 0; i < m_bufferDescriptors.size(); ++i )
    {
        struct v4l2_buffer buf
        {
            .index       = uint32_t( i ),
            .type        = V4L2_BUF_TYPE_VIDEO_CAPTURE,
            .memory      = V4L2_MEMORY_MMAP,
        };

        if( xioctl( m_fd, VIDIOC_QUERYBUF, &buf ) )
        {
            throw std::runtime_error( "VIDIOC_QUERYBUF" );
        }

        m_bufferDescriptors[i].length = buf.length;
        m_bufferDescriptors[i].start = mmap( NULL,
                                             buf.length,
                                             PROT_READ | PROT_WRITE,
                                             MAP_SHARED,
                                             m_fd,
                                             buf.m.offset );

        if( MAP_FAILED == m_bufferDescriptors[i].start )
        {
            throw std::runtime_error( "MAP_FAILED" );
        }
    }
}

/**************************************************************************************************/
void Camera::destroyBuffers()
{
    for( size_t i = 0; i < m_bufferDescriptors.size(); ++i )
    {
        if( munmap( m_bufferDescriptors[i].start,
                    m_bufferDescriptors[i].length ) )
        {
            throw std::runtime_error( "munmap" );
        }
    }
}

/**************************************************************************************************/
void Camera::startStreaming()
{

    for( size_t i = 0; i < m_bufferDescriptors.size(); ++i )
    {
        struct v4l2_buffer buffer
        {
            .index = uint32_t( i ),
            .type = V4L2_BUF_TYPE_VIDEO_CAPTURE,
            .memory = V4L2_MEMORY_MMAP,
        };

        if( xioctl( m_fd, VIDIOC_QBUF, &buffer ) )
        {
            throw std::runtime_error( "VIDIOC_QBUF" );
        }
    }

    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if( xioctl( m_fd, VIDIOC_STREAMON, &type ) )
    {
        throw std::runtime_error( "VIDIOC_STREAMON" );
    }
}

/**************************************************************************************************/
void Camera::stopStreaming()
{

    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if( xioctl( m_fd, VIDIOC_STREAMOFF, &type ) )
    {
        throw std::runtime_error( "VIDIOC_STREAMOFF" );
    }
}

/**************************************************************************************************/
void Camera::grabFrame( void *ref, grabCallbackFunc callback )
{
    struct pollfd fds
    {
        m_fd, POLLIN, 0
    };
    int cnt = poll( &fds, 1, 1000 );
    assert( cnt == 1 );
    assert( fds.events & POLLIN );

    // Dequeue a buffer
    struct v4l2_buffer buffer
    {
        .type = V4L2_BUF_TYPE_VIDEO_CAPTURE,
        .memory = V4L2_MEMORY_MMAP
    };

    if( xioctl( m_fd, VIDIOC_DQBUF, &buffer ) )
    {
        throw std::runtime_error( "VIDIOC_DQBUF" );
    }

    assert( buffer.index < m_bufferDescriptors.size() );
    auto start = m_bufferDescriptors[buffer.index].start;

    if( callback )
    {
        callback( ref,
                  ( uint8_t * )start,
                  m_frameSizes[m_frameSizeIndex].discrete.width,
                  m_frameSizes[m_frameSizeIndex].discrete.height,
                  2,
                  m_frameSizes[m_frameSizeIndex].discrete.width * 2,
                  buffer.sequence );
    }

    // Enqueue the buffer again
    if( xioctl( m_fd, VIDIOC_QBUF, &buffer ) )
    {
        throw std::runtime_error( "VIDIOC_QBUF" );
    }
}

/**************************************************************************************************/
static void grabCallback( void *ref,
                          void *dataptr,
                          uint32_t width,
                          uint32_t height,
                          uint32_t colstride,
                          uint32_t rowstride,
                          uint32_t sequence )
{
    uint8_t *flat = new uint8_t[width * height * 4];
    uint8_t *data = ( uint8_t * )dataptr;

    ///
    for( size_t i = 0; i < width * height * 4; ++i )
    {
        flat[i] = 0;
    }

    for( size_t y = 0; y < height; ++y )
    {
        for( size_t x = 0; x < width; ++x )
        {
            uint8_t raw = ( ( uint8_t * )data )[y * rowstride + x * colstride];

            flat[y * width * 4 + x * 4 + 0] = raw;
            flat[y * width * 4 + x * 4 + 1] = raw;
            flat[y * width * 4 + x * 4 + 2] = raw;
            flat[y * width * 4 + x * 4 + 3] = 255;
        }
    }

    stbi_write_png( "intens.png", width, height, 4, flat, width * 4 );

    ///
    for( size_t i = 0; i < width * height * 4; ++i )
    {
        flat[i] = 0;
    }

    for( size_t y = 0; y < height; ++y )
    {
        for( size_t x = 0; x < width; x += 2 )
        {
            uint8_t y1 = data[y * rowstride + x * colstride + 0];
            uint8_t u = data[y * rowstride + x * colstride + 1];
            uint8_t y2 = data[y * rowstride + x * colstride + 2];
            uint8_t v = data[y * rowstride + x * colstride + 3];

            uint8_t r1 = std::min( 255.0, std::max( 0.0, 1.164 * ( y1 - 16 ) + 1.596 * ( v - 128 ) ) );
            uint8_t g1 = std::min( 255.0, std::max( 0.0, 1.164 * ( y1 - 16 ) - 0.813 * ( u - 128 ) - 0.391 * ( v - 128 ) ) );
            uint8_t b1 = std::min( 255.0, std::max( 0.0, 1.164 * ( y1 - 16 ) + 1.596 * ( u - 128 ) ) );
            uint8_t r2 = std::min( 255.0, std::max( 0.0, 1.164 * ( y2 - 16 ) + 1.596 * ( v - 128 ) ) );
            uint8_t g2 = std::min( 255.0, std::max( 0.0, 1.164 * ( y2 - 16 ) - 0.813 * ( u - 128 ) - 0.391 * ( v - 128 ) ) );
            uint8_t b2 = std::min( 255.0, std::max( 0.0, 1.164 * ( y2 - 16 ) + 1.596 * ( u - 128 ) ) );

            flat[y * width * 4 + x * 4 + 0] = r1;
            flat[y * width * 4 + x * 4 + 1] = g1;
            flat[y * width * 4 + x * 4 + 2] = b1;
            flat[y * width * 4 + x * 4 + 3] = 255;

            flat[y * width * 4 + x * 4 + 4] = r2;
            flat[y * width * 4 + x * 4 + 5] = g2;
            flat[y * width * 4 + x * 4 + 6] = b2;
            flat[y * width * 4 + x * 4 + 7] = 255;
        }
    }

    stbi_write_png( "color.png", width, height, 4, flat, width * 4 );
    delete [] flat;
}

