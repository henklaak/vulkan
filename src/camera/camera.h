#pragma once
#include <string>
#include <vector>
#include <libv4l2.h>
#include <linux/videodev2.h>

typedef void ( *grabCallbackFunc )( void *ref,
                                    void *data,
                                    uint32_t width,
                                    uint32_t height,
                                    uint32_t colstride,
                                    uint32_t rowstride,
                                    uint32_t sequence );

class Camera
{
public:
    Camera( const std::string &device );
    virtual ~Camera();

    void grabFrame( void *ref, grabCallbackFunc callback );
    void setBrightnessDelta( int delta );
    void setContrastDelta( int delta );
    void setGainDelta( int delta );
    void setExposureDelta( int delta );
    void setAutoExposure( bool enabled );

    uint32_t width();
    uint32_t height();


private:
    void openDevice();
    void closeDevice();
    void getControls();
    void getFormats();
    void getFrameSizes();
    void getFrameIntervals();
    void setFormat();
    void setupBuffers();
    void destroyBuffers();
    void startStreaming();
    void stopStreaming();

    std::string m_device;
    int m_fd = -1;

    std::vector<struct v4l2_fmtdesc> m_formats;
    size_t m_formatIndex = 0;

    std::vector<struct v4l2_frmsizeenum> m_frameSizes;
    size_t m_frameSizeIndex = 0;

    std::vector<v4l2_frmivalenum> m_frameIntervals;
    size_t m_frameIntervalIndex = 0;


    struct BufferDescriptor
    {
        void   *start;
        size_t  length;
    };

    std::vector<BufferDescriptor> m_bufferDescriptors;
};
