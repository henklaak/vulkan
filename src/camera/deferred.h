#ifndef DEFERRED_H
#define DEFERRED_H


#define OUTPUT_BUFFER_SIZE (16)

#define WIDTH (960)
#define HEIGHT (720)
#define DEPTH (4)

#define BINDING_RESULT (0)
#define BINDING_SAMPLER (1)
#define BINDING_IMAGE (2)

#endif // DEFERRED_H
