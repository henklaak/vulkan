#include "app_camera.h"
#include <cstring>
#include <fstream>
#include "camera.h"
#include "core_log.h"

static App *app;

/**************************************************************************************************/
App::App() : VulkanSdl( "Camera" )
{
    m_cam = new Camera( "/dev/v4l/by-id/usb-046d_09a2_41DE0B10-video-index0" );
    //m_cam = new Camera( "/dev/v4l/by-id/usb-Azurewave_USB2.0_HD_UVC_WebCam_0x0001-video-index0" );
}

/**************************************************************************************************/
App::~App()
{
    delete m_cam;
}


/**************************************************************************************************/
void App::prepare()
{
    grab();
    prepareRenderImagesSamplers();
    prepareRenderPipeline();
    prepareRenderDescriptorSets();
    prepareComputePipeline();
    prepareComputeDescriptorSets();
}

/**************************************************************************************************/
void App::prepareRenderImagesSamplers()
{
    VkSamplerYcbcrConversionCreateInfo convCI
    {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_CREATE_INFO,
        .format = VK_FORMAT_G8B8G8R8_422_UNORM,
        .ycbcrModel = VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_601,
        .ycbcrRange = VK_SAMPLER_YCBCR_RANGE_ITU_FULL,
    };

    VK_CHECK_RESULT( vkCreateSamplerYcbcrConversion(
                         m_device(),
                         &convCI,
                         nullptr,
                         &m_conversion ) );

    VkSamplerYcbcrConversionInfo convinfo
    {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_INFO,
        .conversion = m_conversion,
    };


    VkExtent2D ext
    {
        .width = m_cam->width(),
        .height = m_cam->height()
    };

    m_utils.createImage( VK_FORMAT_G8B8G8R8_422_UNORM,
                         ext,
                         1,
                         1,
                         VK_SAMPLE_COUNT_1_BIT,
                         VK_IMAGE_TILING_OPTIMAL,
                         VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                         VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                         &m_imageTexture,
                         &m_memoryTexture );

    m_utils.createImageView( m_imageTexture,
                             VK_IMAGE_VIEW_TYPE_2D,
                             VK_FORMAT_G8B8G8R8_422_UNORM,
                             VK_IMAGE_ASPECT_COLOR_BIT,
                             1,
                             1,
                             &m_imageViewTexture,
                             &convinfo );

    m_utils.createBuffer( ext.width * ext.height * 2,
                          VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                          VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                          nullptr,
                          &m_bufferStage,
                          &m_memoryStage );

    VkPhysicalDeviceProperties properties{};
    vkGetPhysicalDeviceProperties( m_physicalDevice(), &properties );

    VkSamplerCreateInfo samplerInfo
    {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .pNext = &convinfo,
        .magFilter = VK_FILTER_LINEAR,
        .minFilter = VK_FILTER_LINEAR,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .anisotropyEnable = VK_FALSE,
        .maxAnisotropy = properties.limits.maxSamplerAnisotropy,
        .compareEnable = VK_FALSE,
        .compareOp = VK_COMPARE_OP_ALWAYS,
        .borderColor = VK_BORDER_COLOR_INT_TRANSPARENT_BLACK,
        .unnormalizedCoordinates = VK_FALSE,
    };

    VK_CHECK_RESULT( vkCreateSampler( m_device(), &samplerInfo, nullptr, &m_samplerTexture ) );

    //  OUTPUT
    m_utils.createImage( VK_FORMAT_B8G8R8A8_UNORM,
                         ext,
                         1,
                         1,
                         VK_SAMPLE_COUNT_1_BIT,
                         VK_IMAGE_TILING_OPTIMAL,
                         VK_IMAGE_USAGE_STORAGE_BIT,
                         VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                         &m_imageOut,
                         &m_memoryOut );

    m_utils.createImageView( m_imageOut,
                             VK_IMAGE_VIEW_TYPE_2D,
                             VK_FORMAT_B8G8R8A8_UNORM,
                             VK_IMAGE_ASPECT_COLOR_BIT,
                             1,
                             1,
                             &m_imageViewOut,
                             &convinfo );
}

/**************************************************************************************************/
void App::prepareRenderPipeline()
{
    auto vertShaderCode = m_utils.readFile( "deferred.vert.spv" );
    auto fragShaderCode = m_utils.readFile( "deferred.frag.spv" );

    VkShaderModule vertShaderModule = m_utils.createShaderModule( vertShaderCode );
    VkShaderModule fragShaderModule = m_utils.createShaderModule( fragShaderCode );

    VkPipelineShaderStageCreateInfo vertShaderStageInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = VK_SHADER_STAGE_VERTEX_BIT,
        .module = vertShaderModule,
        .pName = "main",
    };

    VkPipelineShaderStageCreateInfo fragShaderStageInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
        .module = fragShaderModule,
        .pName = "main",
    };

    VkPipelineShaderStageCreateInfo shaderStages[] = {vertShaderStageInfo, fragShaderStageInfo};

    VkPipelineVertexInputStateCreateInfo vertexInputInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = 0,
    };

    VkPipelineInputAssemblyStateCreateInfo inputAssembly
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP,
        .primitiveRestartEnable = VK_FALSE,
    };

    VkPipelineViewportStateCreateInfo viewportState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .viewportCount = 1,
        .scissorCount = 1,
    };

    VkPipelineRasterizationStateCreateInfo rasterizer
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .cullMode = VK_CULL_MODE_BACK_BIT,
        .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
        .lineWidth = 1.0f,
    };

    VkPipelineMultisampleStateCreateInfo multisampling
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .rasterizationSamples = m_physicalDevice.msaa(),
        .sampleShadingEnable = VK_FALSE,
    };

    VkPipelineDepthStencilStateCreateInfo depthStencil
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = VK_TRUE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_GREATER,
        .depthBoundsTestEnable = VK_FALSE,
        .stencilTestEnable = VK_FALSE,
    };

    VkPipelineColorBlendAttachmentState colorBlendAttachment
    {
        .blendEnable = VK_FALSE,
        .colorWriteMask =
        VK_COLOR_COMPONENT_R_BIT |
        VK_COLOR_COMPONENT_G_BIT |
        VK_COLOR_COMPONENT_B_BIT |
        VK_COLOR_COMPONENT_A_BIT,
    };

    VkPipelineColorBlendStateCreateInfo colorBlending
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .logicOpEnable = VK_FALSE,
        .logicOp = VK_LOGIC_OP_COPY,
        .attachmentCount = 1,
        .pAttachments = &colorBlendAttachment,
        .blendConstants = {0.0f, 0.0f, 0.0f, 0.0f},
    };

    std::vector<VkDynamicState> dynamicStates =
    {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR
    };
    VkPipelineDynamicStateCreateInfo dynamicState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .dynamicStateCount = static_cast<uint32_t>( dynamicStates.size() ),
        .pDynamicStates = dynamicStates.data(),
    };

    //
    VkDescriptorSetLayoutBinding samplerLayoutBinding
    {
        .binding = 0,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = &m_samplerTexture,
    };

    std::array<VkDescriptorSetLayoutBinding, 1> bindings =
    {
        samplerLayoutBinding
    };
    VkDescriptorSetLayoutCreateInfo layoutInfo
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .bindingCount = static_cast<uint32_t>( bindings.size() ),
        .pBindings = bindings.data(),
    };

    if( vkCreateDescriptorSetLayout( m_device(), &layoutInfo, nullptr, &m_descriptorSetLayoutRender ) != VK_SUCCESS )
    {
        throw std::runtime_error( "failed to create descriptor set layout!" );
    }

    VkPipelineLayoutCreateInfo pipelineLayoutInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .setLayoutCount = 1,
        .pSetLayouts = &m_descriptorSetLayoutRender,
    };

    VK_CHECK_RESULT( vkCreatePipelineLayout( m_device(),
                     &pipelineLayoutInfo,
                     nullptr,
                     &m_pipelineLayoutRender ) );

    //
    VkGraphicsPipelineCreateInfo pipelineInfo
    {
        .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .stageCount = 2,
        .pStages = shaderStages,
        .pVertexInputState = &vertexInputInfo,
        .pInputAssemblyState = &inputAssembly,
        .pViewportState = &viewportState,
        .pRasterizationState = &rasterizer,
        .pMultisampleState = &multisampling,
        .pDepthStencilState = &depthStencil,
        .pColorBlendState = &colorBlending,
        .pDynamicState = &dynamicState,
        .layout = m_pipelineLayoutRender,
        .renderPass = m_renderPass.renderPassSwapchain(),
    };
    VK_CHECK_RESULT( vkCreateGraphicsPipelines( m_device(),
                     VK_NULL_HANDLE,
                     1,
                     &pipelineInfo,
                     nullptr,
                     &m_pipelineRender ) );

    vkDestroyShaderModule( m_device(), fragShaderModule, nullptr );
    vkDestroyShaderModule( m_device(), vertShaderModule, nullptr );
}

/**************************************************************************************************/
void App::prepareRenderDescriptorSets()
{
    std::array<VkDescriptorPoolSize, 1> poolSizes
    {
        VkDescriptorPoolSize{
            .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .descriptorCount = static_cast<uint32_t>( MAX_FRAMES_IN_FLIGHT ),
        }
    };

    VkDescriptorPoolCreateInfo poolInfo
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .maxSets = static_cast<uint32_t>( MAX_FRAMES_IN_FLIGHT ),
        .poolSizeCount = static_cast<uint32_t>( poolSizes.size() ),
        .pPoolSizes = poolSizes.data(),
    };

    VK_CHECK_RESULT( vkCreateDescriptorPool( m_device(), &poolInfo, nullptr, &m_descriptorPoolRender ) );

    std::vector<VkDescriptorSetLayout> layouts( MAX_FRAMES_IN_FLIGHT, m_descriptorSetLayoutRender );
    VkDescriptorSetAllocateInfo allocInfo
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .descriptorPool = m_descriptorPoolRender,
        .descriptorSetCount = static_cast<uint32_t>( MAX_FRAMES_IN_FLIGHT ),
        .pSetLayouts = layouts.data(),
    };

    VK_CHECK_RESULT( vkAllocateDescriptorSets( m_device(), &allocInfo, m_descriptorSetsRender.data() ) );

    updateRenderDescriptors();
}

/**************************************************************************************************/
void App::updateRenderDescriptors()
{
    for( size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i )
    {
        VkDescriptorImageInfo imageInfo
        {
            .sampler = m_samplerTexture,
            .imageView = m_imageViewTexture,
            .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        };

        std::array<VkWriteDescriptorSet, 1> descriptorWrites
        {
            VkWriteDescriptorSet{
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .dstSet = m_descriptorSetsRender[i],
                .dstBinding = 0,
                .dstArrayElement = 0,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .pImageInfo = &imageInfo,
            }
        };

        vkUpdateDescriptorSets( m_device(), static_cast<uint32_t>( descriptorWrites.size() ), descriptorWrites.data(), 0,
                                nullptr );
    }
}

/**************************************************************************************************/
void App::prepareComputePipeline()
{
    std::array<VkDescriptorSetLayoutBinding, 3> bindings
    {
        VkDescriptorSetLayoutBinding{
            .binding = 0,
            .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            .descriptorCount = 1,
            .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
        },
        VkDescriptorSetLayoutBinding
        {
            .binding = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .descriptorCount = 1,
            .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
            .pImmutableSamplers = &m_samplerTexture,
        },
        VkDescriptorSetLayoutBinding
        {
            .binding = 2,
            .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
            .descriptorCount = 1,
            .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
        }
    };

    VkDescriptorSetLayoutCreateInfo descriptorLayout
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .bindingCount = bindings.size(),
        .pBindings = bindings.data(),
    };

    VK_CHECK_RESULT( vkCreateDescriptorSetLayout( m_device(), &descriptorLayout, nullptr, &m_descriptorSetLayoutCompute ) );

    VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .setLayoutCount = 1,
        .pSetLayouts = &m_descriptorSetLayoutCompute,
    };
    VK_CHECK_RESULT( vkCreatePipelineLayout( m_device(), &pipelineLayoutCreateInfo, nullptr, &m_pipelineLayoutCompute ) );

    auto computeShaderCode = m_utils.readFile( "deferred.comp.spv" );
    VkShaderModule computeShaderModule = m_utils.createShaderModule( computeShaderCode );

    VkPipelineShaderStageCreateInfo shaderStage =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = VK_SHADER_STAGE_COMPUTE_BIT,
        .module = computeShaderModule,
        .pName = "main",
    };

    VkComputePipelineCreateInfo pipelineCI
    {
        .sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
        .stage = shaderStage,
        .layout = m_pipelineLayoutCompute,
    };

    VK_CHECK_RESULT( vkCreateComputePipelines( m_device(), nullptr, 1, &pipelineCI, nullptr,
                     &m_pipelineCompute ) );

    vkDestroyShaderModule( m_device(), computeShaderModule, nullptr );
}

/**************************************************************************************************/
void App::prepareComputeDescriptorSets()
{
    std::array<VkDescriptorPoolSize, 1> poolSizes
    {
        VkDescriptorPoolSize{
            .type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            .descriptorCount = static_cast<uint32_t>( MAX_FRAMES_IN_FLIGHT ),
        }
    };

    VkDescriptorPoolCreateInfo poolInfo
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .maxSets = static_cast<uint32_t>( MAX_FRAMES_IN_FLIGHT ),
        .poolSizeCount = static_cast<uint32_t>( poolSizes.size() ),
        .pPoolSizes = poolSizes.data(),
    };

    VK_CHECK_RESULT( vkCreateDescriptorPool( m_device(), &poolInfo, nullptr, &m_descriptorPoolCompute ) );

    std::vector<VkDescriptorSetLayout> layouts( MAX_FRAMES_IN_FLIGHT, m_descriptorSetLayoutCompute );
    VkDescriptorSetAllocateInfo allocInfo
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .descriptorPool = m_descriptorPoolCompute,
        .descriptorSetCount = static_cast<uint32_t>( MAX_FRAMES_IN_FLIGHT ),
        .pSetLayouts = layouts.data(),
    };

    VK_CHECK_RESULT( vkAllocateDescriptorSets( m_device(), &allocInfo, m_descriptorSetsCompute.data() ) );

    for( size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i )
    {
        m_utils.createBuffer( m_cam->height() * 16 * 8,
                              VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                              VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                              nullptr,
                              &m_buffersResult[i],
                              &m_memoriesResult[i] );
    }

    updateComputeDescriptors();
}

/**************************************************************************************************/
void App::updateComputeDescriptors()
{
    VkDescriptorBufferInfo bufferInfo
    {
        .buffer = m_buffersResult[m_currentFrame],
        .offset = 0,
        .range = m_cam->height() * 16 * 8,
    };

    VkDescriptorImageInfo imageInfo
    {
        .sampler = m_samplerTexture,
        .imageView = m_imageViewTexture,
        .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
    };

    VkDescriptorImageInfo imageInfo2
    {
        .imageView = m_imageViewOut,
        .imageLayout = VK_IMAGE_LAYOUT_GENERAL,
    };

    std::array<VkWriteDescriptorSet, 3> descriptorWrites
    {
        VkWriteDescriptorSet{
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = m_descriptorSetsCompute[m_currentFrame],
            .dstBinding = 0,
            .dstArrayElement = 0,
            .descriptorCount = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            .pBufferInfo = &bufferInfo,
        },
        VkWriteDescriptorSet{
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = m_descriptorSetsCompute[m_currentFrame],
            .dstBinding = 1,
            .dstArrayElement = 0,
            .descriptorCount = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .pImageInfo = &imageInfo,
        },
        VkWriteDescriptorSet{
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = m_descriptorSetsCompute[m_currentFrame],
            .dstBinding = 2,
            .dstArrayElement = 0,
            .descriptorCount = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
            .pImageInfo = &imageInfo2,
        }
    };

    vkUpdateDescriptorSets( m_device(), static_cast<uint32_t>( descriptorWrites.size() ), descriptorWrites.data(), 0,
                            nullptr );
}
/**************************************************************************************************/
void App::unprepare()
{
    for( size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i )
    {
        m_utils.destroyBuffer( &m_buffersResult[i], &m_memoriesResult[i] );
    }

    vkDestroyDescriptorPool( m_device(), m_descriptorPoolCompute, nullptr );
    vkDestroyDescriptorSetLayout( m_device(), m_descriptorSetLayoutCompute, nullptr );
    vkDestroyPipeline( m_device(), m_pipelineCompute, nullptr );
    vkDestroyPipelineLayout( m_device(), m_pipelineLayoutCompute, nullptr );

    vkDestroyDescriptorPool( m_device(), m_descriptorPoolRender, nullptr );
    vkDestroyDescriptorSetLayout( m_device(), m_descriptorSetLayoutRender, nullptr );
    vkDestroyPipeline( m_device(), m_pipelineRender, nullptr );
    vkDestroyPipelineLayout( m_device(), m_pipelineLayoutRender, nullptr );
    vkDestroySampler( m_device(), m_samplerTexture, nullptr );
    m_utils.destroyImageView( &m_imageViewOut );
    m_utils.destroyImage( &m_imageOut, &m_memoryOut );
    m_utils.destroyImageView( &m_imageViewTexture );
    m_utils.destroyImage( &m_imageTexture, &m_memoryTexture );
    vkDestroySamplerYcbcrConversion( m_device(), m_conversion, nullptr );
    m_utils.destroyBuffer( &m_bufferStage, &m_memoryStage );
}

/**************************************************************************************************/
void App::fillCommandBufferPreRenderPass()
{
    app->refreshTextureImage();

    vkCmdBindPipeline( m_commandBuffers[m_currentFrame],
                       VK_PIPELINE_BIND_POINT_COMPUTE,
                       m_pipelineCompute );
    vkCmdBindDescriptorSets( m_commandBuffers[m_currentFrame],
                             VK_PIPELINE_BIND_POINT_COMPUTE,
                             m_pipelineLayoutCompute,
                             0,
                             1,
                             &m_descriptorSetsCompute[m_currentFrame],
                             0,
                             0 );
    vkCmdDispatch( m_commandBuffers[m_currentFrame], m_cam->width(), 1, 1 );
}

/**************************************************************************************************/
void App::fillCommandBuffer()
{
    vkCmdBindPipeline( m_commandBuffers[m_currentFrame],
                       VK_PIPELINE_BIND_POINT_GRAPHICS,
                       m_pipelineRender );

    vkCmdBindDescriptorSets( m_commandBuffers[m_currentFrame],
                             VK_PIPELINE_BIND_POINT_GRAPHICS,
                             m_pipelineLayoutRender,
                             0,
                             1,
                             &m_descriptorSetsRender[m_currentFrame],
                             0,
                             nullptr );
    vkCmdDraw( m_commandBuffers[m_currentFrame], 4, 1, 0, 0 );
}


/**************************************************************************************************/
static void grabCallback( void *ref,
                          void *dataptr,
                          uint32_t width,
                          uint32_t height,
                          uint32_t colstride,
                          uint32_t rowstride,
                          uint32_t sequence )
{
    App *app = reinterpret_cast<App *>( ref );
    app->grabCB( dataptr, width, height );
}

/**************************************************************************************************/
void App::grab()
{
    m_cam->grabFrame( this, grabCallback );
}

/**************************************************************************************************/
void App::grabCB( void *dataptr,
                  uint32_t width,
                  uint32_t height )
{
    if( m_memoryStage )
    {
        void *orgptr;
        m_utils.mapBuffer( m_memoryStage, &orgptr );
        memcpy( orgptr, dataptr, m_cam->width() * m_cam->height() * 2 );
        m_utils.unmapBuffer( m_memoryStage, &orgptr );
    }
}

/**************************************************************************************************/
void App::refreshTextureImage()
{
    m_utils.transitionImageLayout( m_commandBuffers[m_currentFrame], m_imageTexture,
                                   VK_IMAGE_LAYOUT_UNDEFINED,
                                   VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL );

    m_utils.copyBufferToImage( m_commandBuffers[m_currentFrame], m_bufferStage, m_imageTexture, m_cam->width(),
                               m_cam->height() );

    m_utils.transitionImageLayout( m_commandBuffers[m_currentFrame], m_imageTexture,
                                   VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                   VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL );

    m_utils.transitionImageLayout( m_commandBuffers[m_currentFrame], m_imageOut,
                                   VK_IMAGE_LAYOUT_UNDEFINED,
                                   VK_IMAGE_LAYOUT_GENERAL );

}


/**************************************************************************************************/
int main( int, char *[] )
{
    app = new App();
    app->create();

    int i = 0;

    while( app->m_window.pollWindow() )
    {
        app->grab();
        app->acquireNextFrame();
        app->renderNextFrame();
        app->presentNextFrame();

        if( i++ >= 100 )
        {
            break;
        }
    }

    app->destroy();
    delete( app );
    app = nullptr;

    std::cout << "Done" << std::endl;
    return 0;
}
