#include "gltf_node.h"
#include "gltf_context.h"

/**************************************************************************************************/
GltfNode::GltfNode( GltfContext *context )
    : m_context( context )
{}


/**************************************************************************************************/
GltfNode::~GltfNode()
{}


/**************************************************************************************************/
void GltfNode::create( int nodeNr )
{
    tinygltf::Node &node = m_context->model.nodes[nodeNr];

    // TODO position


    int meshNr = node.mesh;

    if( meshNr < 0 )
    {
        return;
    }


    GltfMesh mesh( m_context );

    mesh.create( meshNr );
    m_meshes.push_back( mesh );
}

/**************************************************************************************************/
void GltfNode::destroy()
{
    for( auto &mesh : m_meshes )
    {
        mesh.destroy();
    }
}

/**************************************************************************************************/
void GltfNode::fillCommandBuffer( VkCommandBuffer cmdBuffer )
{
    for( auto &mesh : m_meshes )
    {
        mesh.fillCommandBuffer( cmdBuffer );
    }
}

