#include "materialless_pipeline.h"
#include <array>
#include "core_log.h"
#include "core_utils.h"

#include "materialless_constants.h"

/**************************************************************************************************/
MateriallessPipeline::MateriallessPipeline()
{}

/**************************************************************************************************/
MateriallessPipeline::~MateriallessPipeline()
{}

/**************************************************************************************************/
void MateriallessPipeline::create( Utils *utils,
                                   VkDevice device,
                                   VkSampleCountFlagBits msaa,
                                   VkRenderPass renderPass )
{
    m_utils = utils;
    m_device = device;

    auto vertShaderCode = m_utils->readFile( "materialless.vert.spv" );
    auto fragShaderCode = m_utils->readFile( "materialless.frag.spv" );

    VkShaderModule vertShaderModule = m_utils->createShaderModule( vertShaderCode );
    VkShaderModule fragShaderModule = m_utils->createShaderModule( fragShaderCode );

    VkPipelineShaderStageCreateInfo vertShaderStageInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = VK_SHADER_STAGE_VERTEX_BIT,
        .module = vertShaderModule,
        .pName = "main"
    };

    VkPipelineShaderStageCreateInfo fragShaderStageInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
        .module = fragShaderModule,
        .pName = "main"
    };

    VkPipelineShaderStageCreateInfo shaderStages[] =
    {
        vertShaderStageInfo,
        fragShaderStageInfo
    };

    VkVertexInputBindingDescription binding
    {
        .binding = 0,
        .stride = 3 * sizeof( float ),
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
    };

    VkVertexInputAttributeDescription attrib
    {
        .location = LOCATION_VERTICES,
        .binding = 0,
        .format =  VK_FORMAT_R32G32B32_SFLOAT,
        .offset = 0,
    };

    VkPipelineVertexInputStateCreateInfo vertexInputInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = 1,
        .pVertexBindingDescriptions = &binding,
        .vertexAttributeDescriptionCount = 1,
        .pVertexAttributeDescriptions = &attrib,
    };

    VkPipelineInputAssemblyStateCreateInfo inputAssembly
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        .primitiveRestartEnable = VK_FALSE,
    };

    VkPipelineViewportStateCreateInfo viewportState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .viewportCount = 1,
        .scissorCount = 1,
    };

    VkPipelineRasterizationStateCreateInfo rasterizer
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .cullMode = VK_CULL_MODE_BACK_BIT,
        .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
        .lineWidth = 1.0f,
    };

    VkPipelineMultisampleStateCreateInfo multisampling
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .rasterizationSamples = msaa,
        .sampleShadingEnable = VK_FALSE,
    };

    VkPipelineDepthStencilStateCreateInfo depthStencil
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = VK_TRUE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_GREATER,
        .depthBoundsTestEnable = VK_FALSE,
        .stencilTestEnable = VK_FALSE,
    };

    VkPipelineColorBlendAttachmentState colorBlendAttachment
    {
        .blendEnable = VK_FALSE,
        .colorWriteMask =
        VK_COLOR_COMPONENT_R_BIT |
        VK_COLOR_COMPONENT_G_BIT |
        VK_COLOR_COMPONENT_B_BIT |
        VK_COLOR_COMPONENT_A_BIT,
    };

    VkPipelineColorBlendStateCreateInfo colorBlending
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .logicOpEnable = VK_FALSE,
        .logicOp = VK_LOGIC_OP_COPY,
        .attachmentCount = 1,
        .pAttachments = &colorBlendAttachment,
        .blendConstants = {0.0f, 0.0f, 0.0f, 0.0f},
    };

    std::vector<VkDynamicState> dynamicStates =
    {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR
    };

    VkPipelineDynamicStateCreateInfo dynamicState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .dynamicStateCount = static_cast<uint32_t>( dynamicStates.size() ),
        .pDynamicStates = dynamicStates.data(),
    };

    VkDescriptorSetLayoutBinding descriptorsetLayoutBinding
    {
        .binding = BINDING_UBO,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        .pImmutableSamplers = nullptr,
    };

    VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCI
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .bindingCount = 1,
        .pBindings = &descriptorsetLayoutBinding,
    };
    VK_CHECK_RESULT( vkCreateDescriptorSetLayout( m_device, &descriptorSetLayoutCI, nullptr, &m_descriptorSetLayout ) );

    VkPipelineLayoutCreateInfo pipelineLayoutInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .setLayoutCount = 1,
        .pSetLayouts = &m_descriptorSetLayout,
    };
    VK_CHECK_RESULT( vkCreatePipelineLayout( m_device,
                     &pipelineLayoutInfo,
                     nullptr,
                     &m_pipelineLayoutRender ) );

    std::array < VkGraphicsPipelineCreateInfo, 1> pipelineInfos =
    {
        VkGraphicsPipelineCreateInfo{
            .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
            .stageCount = 2,
            .pStages = shaderStages,
            .pVertexInputState = &vertexInputInfo,
            .pInputAssemblyState = &inputAssembly,
            .pViewportState = &viewportState,
            .pRasterizationState = &rasterizer,
            .pMultisampleState = &multisampling,
            .pDepthStencilState = &depthStencil,
            .pColorBlendState = &colorBlending,
            .pDynamicState = &dynamicState,
            .layout = m_pipelineLayoutRender,
            .renderPass = renderPass,
        }
    };
    VK_CHECK_RESULT( vkCreateGraphicsPipelines( m_device,
                     VK_NULL_HANDLE,
                     pipelineInfos.size(),
                     pipelineInfos.data(),
                     nullptr,
                     &m_pipelineRender ) );

    vkDestroyShaderModule( m_device, fragShaderModule, nullptr );
    vkDestroyShaderModule( m_device, vertShaderModule, nullptr );
}

/**************************************************************************************************/
void MateriallessPipeline::destroy()
{
    vkDestroyDescriptorSetLayout( m_device, m_descriptorSetLayout, nullptr );
    vkDestroyPipeline( m_device, m_pipelineRender, nullptr );
    vkDestroyPipelineLayout( m_device, m_pipelineLayoutRender, nullptr );
}
