#version 450

#extension GL_GOOGLE_include_directive : require
#include "materialless_constants.h"

layout (location=LOCATION_VERTICES) in vec3 inPos;

layout (binding=BINDING_UBO) uniform UBO
{
    mat4 projection;
    mat4 view;
    mat4 model;
} ubo;

void main()
{
    gl_Position =  ubo.projection * ubo.view * ubo.model * vec4(inPos,1);
}
