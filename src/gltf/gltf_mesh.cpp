#include "gltf_mesh.h"
#include "gltf_context.h"
#include "gltf_primitive.h"

/**************************************************************************************************/
GltfMesh::GltfMesh( GltfContext *context )
    : m_context( context )
{}


/**************************************************************************************************/
GltfMesh::~GltfMesh()
{}


/**************************************************************************************************/
void GltfMesh::create( int  meshNr )
{
    tinygltf::Mesh &node = m_context->model.meshes[meshNr];

    for( auto &primitive : node.primitives )
    {
        GltfPrimitive prim( m_context );
        prim.create( primitive );
        m_primitives.push_back( prim );
    }
}

/**************************************************************************************************/
void GltfMesh::destroy()
{
    for( auto &prim : m_primitives )
    {
        prim.destroy();
    }
}

/**************************************************************************************************/
void GltfMesh::fillCommandBuffer( VkCommandBuffer cmdBuffer )
{
    for( auto &prim : m_primitives )
    {
        prim.fillCommandBuffer( cmdBuffer );
    }
}

