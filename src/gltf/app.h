#pragma once
#include "sdl_basis.h"

class GltfContext;
class GltfScene;

class App: public VulkanSdl
{
public:
    App();
    virtual ~App();

    void prepare() override;
    void prepareDescriptorPool();

    void unprepare() override;

    void fillCommandBufferPreRenderPass() override;
    void fillCommandBuffer() override;

    VkDescriptorPool m_descriptorPool = VK_NULL_HANDLE;

    GltfContext *m_context;
    GltfScene *m_scene;
};
