#include "gltf_scene.h"
#include "core_utils.h"
#include "gltf_context.h"
#include "gltf_node.h"

/**************************************************************************************************/
GltfScene::GltfScene( GltfContext *context )
    :  m_context( context )
{
}

/**************************************************************************************************/
GltfScene::~GltfScene()
{}

/**************************************************************************************************/
void GltfScene::create( const std::string &filename )
{
    m_context->utils->loadGltfModel( filename, &m_context->model );

    tinygltf::Scene &defaultScene = m_context->model.scenes[m_context->model.defaultScene];

    for( int nodeNr : defaultScene.nodes )
    {
        GltfNode mynode( m_context );

        mynode.create( nodeNr );
        m_nodes.push_back( mynode );
    }
}


/**************************************************************************************************/
void GltfScene::destroy()
{
    for( auto &node : m_nodes )
    {
        node.destroy();
    }
}


/**************************************************************************************************/
void GltfScene::fillCommandBuffer( VkCommandBuffer cmdBuffer )
{
    for( auto &node : m_nodes )
    {
        node.fillCommandBuffer( cmdBuffer );
    }
}

