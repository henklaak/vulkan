#pragma once
#include <tiny_gltf.h>
#include <vulkan/vulkan.h>
#include <vector>

#include "materialless_pipeline.h"

class GltfContext;

class GltfPrimitive
{
public:
    GltfPrimitive( GltfContext *context );
    virtual ~GltfPrimitive();

    void create( tinygltf::Primitive &primitive );
    void destroy() ;

    void fillCommandBuffer( VkCommandBuffer cmdBuffer ) ;

private:
    GltfContext *m_context = nullptr;
    tinygltf::Primitive *m_primitive = nullptr;

    void createIndexBuffers();
    VkBuffer m_bufferIndex = VK_NULL_HANDLE;
    VkDeviceMemory m_memoryIndex = VK_NULL_HANDLE;

    void createVertexBuffers();
    VkBuffer m_bufferVertex = VK_NULL_HANDLE;
    VkDeviceMemory m_memoryVertex = VK_NULL_HANDLE;

    void createUBOBuffers();
    VkBuffer m_bufferUBO = VK_NULL_HANDLE;
    VkDeviceMemory m_memoryUBO = VK_NULL_HANDLE;

    void createDescriptorSets();
    VkDescriptorSet m_descriptorSet = VK_NULL_HANDLE;

    MateriallessPipeline m_materiallessPipeline;
};
