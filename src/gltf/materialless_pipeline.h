#pragma once
#include <vulkan/vulkan.h>
#include <glm/glm.hpp>

#include "pipeline.h"

class Utils;

class MateriallessPipeline : public Pipeline
{
public:
    MateriallessPipeline();
    virtual ~MateriallessPipeline();

    struct UBOVS
    {
        glm::mat4 projection = glm::mat4( 1 );
        glm::mat4 view = glm::mat4( 1 );
        glm::mat4 model = glm::mat4( 1 );
    };

    void create( Utils *utils,
                 VkDevice device,
                 VkSampleCountFlagBits msaa,
                 VkRenderPass renderPass ) override;
    void destroy() override;
};
