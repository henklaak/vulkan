#include "app.h"
#include <fstream>
#include <thread>

#include <tiny_gltf.h>

#include <glm/gtc/matrix_transform.hpp>

#include "core_log.h"
#include "gltf_context.h"
#include "gltf_scene.h"


static App *app;

/**************************************************************************************************/
App::App() : VulkanSdl( "GLTF" )
{

}

/**************************************************************************************************/
App::~App()
{
}

/**************************************************************************************************/
void App::prepare()
{
    prepareDescriptorPool();

    m_context = new GltfContext( &m_utils,
                                 m_device(),
                                 m_physicalDevice.msaa(),
                                 m_renderPass.renderPassSwapchain(),
                                 m_descriptorPool );

    m_scene = new GltfScene( m_context );
    m_scene->create( "/home/henklaak/Projects/Vulkan/resources/untitled.glb" );
}

/**************************************************************************************************/
void App::unprepare()
{
    m_scene->destroy();
    delete m_scene;
    delete m_context;

    vkDestroyDescriptorPool( m_device(), m_descriptorPool, nullptr );
}

/**************************************************************************************************/
void App::prepareDescriptorPool()
{
    std::array<VkDescriptorPoolSize, 1> poolSizes
    {
        VkDescriptorPoolSize{
            .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = 1000,
        }
    };

    VkDescriptorPoolCreateInfo poolInfo
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .maxSets = static_cast<uint32_t>( 1000 ),
        .poolSizeCount = static_cast<uint32_t>( poolSizes.size() ),
        .pPoolSizes = poolSizes.data(),
    };

    VK_CHECK_RESULT( vkCreateDescriptorPool( m_device(), &poolInfo, nullptr, &m_descriptorPool ) );
}

/**************************************************************************************************/
void App::fillCommandBufferPreRenderPass()
{
}

/**************************************************************************************************/
void App::fillCommandBuffer()
{
    m_scene->fillCommandBuffer( m_commandBuffers[m_currentFrame] );
}


/**************************************************************************************************/
int main( int, char *[] )
{
    app = new App();
    app->create();

    int i = 0;

    while( app->m_window.pollWindow() )
    {
        if( app->acquireNextFrame() )
        {
            app->renderNextFrame();
            app->presentNextFrame();
        }
    }

    app->destroy();
    delete( app );
    app = nullptr;

    std::cout << "Done" << std::endl;
    return 0;
}
