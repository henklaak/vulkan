#pragma once
#include <vulkan/vulkan.h>
#include <tiny_gltf.h>

class Utils;

class GltfContext
{
public:
    GltfContext( Utils *utils,
                 VkDevice device,
                 VkSampleCountFlagBits msaa,
                 VkRenderPass renderPass,
                 VkDescriptorPool descriptorPool );
    virtual ~GltfContext();

    Utils *utils = nullptr;
    VkDevice device = VK_NULL_HANDLE;
    VkSampleCountFlagBits msaa = VK_SAMPLE_COUNT_1_BIT;
    VkRenderPass renderPass = VK_NULL_HANDLE;
    VkDescriptorPool descriptorPool = VK_NULL_HANDLE;
    tinygltf::Model model;
};
