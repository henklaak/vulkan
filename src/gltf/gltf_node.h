#pragma once
#include <tiny_gltf.h>
#include <vulkan/vulkan.h>
#include "gltf_mesh.h"

class GltfContext;

class GltfNode
{
public:
    GltfNode( GltfContext *context );
    virtual ~GltfNode();

    void create( int node_nr ) ;
    void destroy() ;

    void fillCommandBuffer( VkCommandBuffer cmdBuffer ) ;

private:
    GltfContext *m_context = nullptr;

    std::vector<GltfMesh> m_meshes;
};
