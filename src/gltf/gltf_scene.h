#pragma once
#include <vulkan/vulkan.h>
#include <tiny_gltf.h>
#include <string>

#include "gltf_node.h"

class GltfContext;

class GltfScene
{
public:
    GltfScene( GltfContext *context );
    virtual ~GltfScene();

    void create( const std::string &filename ) ;
    void destroy() ;

    void fillCommandBuffer( VkCommandBuffer cmdBuffer ) ;

private:
    GltfContext *m_context;

    std::vector<GltfNode> m_nodes;
};
