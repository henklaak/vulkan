#include "gltf_primitive.h"


#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "gltf_context.h"
#include "core_log.h"
#include "core_utils.h"
#include "materialless_constants.h"


/**************************************************************************************************/
GltfPrimitive::GltfPrimitive( GltfContext *context )
    : m_context( context )
{}

/**************************************************************************************************/
GltfPrimitive::~GltfPrimitive()
{}


/**************************************************************************************************/
void GltfPrimitive::create( tinygltf::Primitive &primitive )
{
    m_primitive = &primitive;

    m_materiallessPipeline.create( m_context->utils,
                                   m_context->device,
                                   m_context->msaa,
                                   m_context->renderPass );

    createIndexBuffers();
    createVertexBuffers();
    createUBOBuffers();
    createDescriptorSets( );
}

/**************************************************************************************************/
void GltfPrimitive::destroy()
{
    m_context->utils->destroyBuffer( &m_bufferUBO, &m_memoryUBO );
    m_context->utils->destroyBuffer( &m_bufferVertex, &m_memoryVertex );
    m_context->utils->destroyBuffer( &m_bufferIndex, &m_memoryIndex );

    m_materiallessPipeline.destroy();
}

/**************************************************************************************************/
void GltfPrimitive::createIndexBuffers( )
{
    int acc_indices = m_primitive->indices;
    tinygltf::Accessor &acc = m_context->model.accessors[acc_indices];

    assert( acc.componentType == TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT );
    assert( acc.type == TINYGLTF_TYPE_SCALAR );
    assert( acc.normalized == false );

    int bufferViewIndex = acc.bufferView;

    tinygltf::BufferView &bufferView = m_context->model.bufferViews[bufferViewIndex];

    int bufferIndex = bufferView.buffer;
    tinygltf::Buffer &buffer = m_context->model.buffers[bufferIndex];


    VkBuffer stagingBuffer;
    VkDeviceMemory stagingMemory;
    VkDeviceSize stagingSize = acc.count * sizeof( uint16_t );

    m_context->utils->createBuffer( stagingSize,
                                    VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                                    VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                                    buffer.data.data() + bufferView.byteOffset + acc.byteOffset,
                                    &stagingBuffer,
                                    &stagingMemory );

    m_context->utils->createBuffer( stagingSize,
                                    VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                                    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                                    nullptr,
                                    &m_bufferIndex,
                                    &m_memoryIndex );

    m_context->utils->copyBuffer( stagingBuffer, m_bufferIndex, stagingSize );

    m_context->utils->destroyBuffer( &stagingBuffer, &stagingMemory );
}

/**************************************************************************************************/
void GltfPrimitive::createVertexBuffers()
{
    assert( m_primitive->mode == TINYGLTF_MODE_TRIANGLES );

    int acc_position =  m_primitive->attributes["POSTION"];
    tinygltf::Accessor &acc = m_context->model.accessors[acc_position];

    assert( acc.componentType == TINYGLTF_COMPONENT_TYPE_FLOAT );
    assert( acc.type == TINYGLTF_TYPE_VEC3 );
    assert( acc.normalized == false );

    int bufferViewIndex = acc.bufferView;

    tinygltf::BufferView &bufferView = m_context->model.bufferViews[bufferViewIndex];

    int bufferIndex = bufferView.buffer;
    tinygltf::Buffer &buffer = m_context->model.buffers[bufferIndex];


    VkBuffer stagingBuffer;
    VkDeviceMemory stagingMemory;
    VkDeviceSize stagingSize = acc.count * 3 * sizeof( float );

    m_context->utils->createBuffer( stagingSize,
                                    VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                                    VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                                    buffer.data.data() + bufferView.byteOffset + acc.byteOffset,
                                    &stagingBuffer,
                                    &stagingMemory );

    m_context->utils->createBuffer( stagingSize,
                                    VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                                    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                                    nullptr,
                                    &m_bufferVertex,
                                    &m_memoryVertex );

    m_context->utils->copyBuffer( stagingBuffer, m_bufferVertex, stagingSize );

    m_context->utils->destroyBuffer( &stagingBuffer, &stagingMemory );
}


/**************************************************************************************************/
void GltfPrimitive::createUBOBuffers()
{
    MateriallessPipeline::UBOVS uboVS
    {
        .projection = glm::perspective( glm::radians( 45.0f ), 1280.0f / 720.0f, 1000.0f, 0.01f ),
        .model = glm::lookAt( glm::vec3( 6.0f, 6.0f, 6.0f ),
                              glm::vec3( 0.0f, 0.0f, 0.0f ),
                              glm::vec3( 0.0f, 1.0f, 0.0f ) )
    };

    uboVS.projection[1][1] *= -1.0;

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingMemory;
    VkDeviceSize stagingSize = sizeof( uboVS );

    m_context->utils->createBuffer( stagingSize,
                                    VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                                    VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                                    &uboVS,
                                    &stagingBuffer,
                                    &stagingMemory );

    m_context->utils->createBuffer( stagingSize,
                                    VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                                    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                                    nullptr,
                                    &m_bufferUBO,
                                    &m_memoryUBO );

    m_context->utils->copyBuffer( stagingBuffer, m_bufferUBO, stagingSize );

    m_context->utils->destroyBuffer( &stagingBuffer, &stagingMemory );
}

/**************************************************************************************************/
void GltfPrimitive::createDescriptorSets()
{
    std::array<VkDescriptorSetLayout, 1> layouts = {m_materiallessPipeline.descriptorSetlayout()};

    VkDescriptorSetAllocateInfo allocInfo
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .descriptorPool = m_context->descriptorPool,
        .descriptorSetCount = layouts.size(),
        .pSetLayouts = layouts.data(),
    };

    VK_CHECK_RESULT( vkAllocateDescriptorSets( m_context->device, &allocInfo, &m_descriptorSet ) );

    VkDescriptorBufferInfo bufferInfo
    {
        .buffer = m_bufferUBO,
        .offset = 0,
        .range = sizeof( MateriallessPipeline::UBOVS ),
    };

    std::array<VkWriteDescriptorSet, 1> descriptorWrites
    {
        VkWriteDescriptorSet{

            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = m_descriptorSet,
            .dstBinding = BINDING_UBO,
            .dstArrayElement = 0,
            .descriptorCount = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .pBufferInfo = &bufferInfo,
        }};

    vkUpdateDescriptorSets( m_context->device,
                            static_cast<uint32_t>( descriptorWrites.size() ),
                            descriptorWrites.data(),
                            0,
                            nullptr );
}

/**************************************************************************************************/
void GltfPrimitive::fillCommandBuffer( VkCommandBuffer cmdBuffer )
{
    vkCmdBindPipeline( cmdBuffer,
                       VK_PIPELINE_BIND_POINT_GRAPHICS,
                       m_materiallessPipeline.pipeline() );

    VkDeviceSize offset = 0;
    vkCmdBindIndexBuffer( cmdBuffer,
                          m_bufferIndex,
                          offset,
                          VK_INDEX_TYPE_UINT16 );

    std::array<VkDeviceSize, 1> offsets = {0};

    vkCmdBindVertexBuffers( cmdBuffer,
                            0,
                            1,
                            & m_bufferVertex,
                            offsets.data() );

    vkCmdBindDescriptorSets( cmdBuffer,
                             VK_PIPELINE_BIND_POINT_GRAPHICS,
                             m_materiallessPipeline.layout(),
                             0,
                             1,
                             &m_descriptorSet,
                             0,
                             NULL );

    vkCmdDrawIndexed( cmdBuffer,
                      36,
                      1,
                      0,
                      0,
                      0 );
}

