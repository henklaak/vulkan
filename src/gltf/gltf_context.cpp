#include "gltf_context.h"

GltfContext::GltfContext( Utils *utils_,
                          VkDevice device_,
                          VkSampleCountFlagBits msaa_,
                          VkRenderPass renderPass_,
                          VkDescriptorPool descriptorPool_ )
    : utils( utils_ )
    , device( device_ )
    , msaa( msaa_ )
    , renderPass( renderPass_ )
    , descriptorPool( descriptorPool_ )
{}

GltfContext::~GltfContext()
{}

