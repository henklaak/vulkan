#pragma once
#include <tiny_gltf.h>
#include <vulkan/vulkan.h>
#include "gltf_primitive.h"

class GltfContext;

class GltfMesh
{
public:
    GltfMesh( GltfContext *context );
    virtual ~GltfMesh();

    void create( int meshNr ) ;
    void destroy() ;

    void fillCommandBuffer( VkCommandBuffer cmdBuffer ) ;

private:
    GltfContext *m_context = nullptr;

    std::vector<GltfPrimitive> m_primitives;
};
