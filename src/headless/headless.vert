#version 450

layout (location = 0) in vec4 inPos;

layout (binding = 0) uniform UBO
{
    mat4 projection;
    mat4 model;
    mat4 normal;
} ubo;

layout (location = 0) out vec3 outCol;


out gl_PerVertex {
        vec4 gl_Position;
};

void main()
{
    gl_Position =  ubo.projection * ubo.model * inPos;

    outCol = vec3(gl_VertexIndex/7.0,(7-gl_VertexIndex)/7.0,0.0);
}
