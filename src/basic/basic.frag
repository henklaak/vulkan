#version 450

layout (location = 0) in vec3 outCol;
layout (location = 0) out vec4 outFragColor;

void main()
{
    outFragColor = vec4(outCol, 1.0);
}
