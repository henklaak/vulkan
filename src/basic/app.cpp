#include "app.h"
#include <fstream>
#include "core_log.h"
#include <tiny_gltf.h>
#include <glm/gtc/matrix_transform.hpp>
#include <thread>

static App *app;

/**************************************************************************************************/
App::App() : VulkanSdl( "Basic" )
{
}

/**************************************************************************************************/
App::~App()
{
}

/**************************************************************************************************/
void App::prepare()
{
    prepareRenderPipeline();
    prepareBuffers();
}

/**************************************************************************************************/
void App::unprepare()
{
    vkDestroyDescriptorSetLayout( m_device(), m_descriptorSetLayout, nullptr );
    vkDestroyDescriptorPool( m_device(), m_descriptorPool, nullptr );
    m_utils.destroyBuffer( &m_bufferUBO, &m_memoryUBO );
    m_utils.destroyBuffer( &m_bufferIndex, &m_memoryIndex );
    m_utils.destroyBuffer( &m_bufferVertex, &m_memoryVertex );
    m_utils.destroyBuffer( &m_bufferVertex2, &m_memoryVertex2 );
    vkDestroyPipeline( m_device(), m_pipelineRender, nullptr );
    vkDestroyPipelineLayout( m_device(), m_pipelineLayoutRender, nullptr );
}

/**************************************************************************************************/
void App::prepareRenderPipeline()
{
    auto vertShaderCode = m_utils.readFile( "basic.vert.spv" );
    auto fragShaderCode = m_utils.readFile( "basic.frag.spv" );

    VkShaderModule vertShaderModule = m_utils.createShaderModule( vertShaderCode );
    VkShaderModule fragShaderModule = m_utils.createShaderModule( fragShaderCode );

    VkPipelineShaderStageCreateInfo vertShaderStageInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = VK_SHADER_STAGE_VERTEX_BIT,
        .module = vertShaderModule,
        .pName = "main"
    };

    VkPipelineShaderStageCreateInfo fragShaderStageInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
        .module = fragShaderModule,
        .pName = "main"
    };

    VkPipelineShaderStageCreateInfo shaderStages[] =
    {
        vertShaderStageInfo,
        fragShaderStageInfo
    };

    VkVertexInputBindingDescription binding
    {
        .binding = 0,
        .stride = 4 * sizeof( float ),
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
    };

    VkVertexInputAttributeDescription attrib
    {
        .location = 0,
        .binding = 0,
        .format =  VK_FORMAT_R32G32B32A32_SFLOAT,
        .offset = 0,
    };

    VkPipelineVertexInputStateCreateInfo vertexInputInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = 1,
        .pVertexBindingDescriptions = &binding,
        .vertexAttributeDescriptionCount = 1,
        .pVertexAttributeDescriptions = &attrib,
    };

    VkPipelineInputAssemblyStateCreateInfo inputAssembly
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        .primitiveRestartEnable = VK_FALSE,
    };

    VkPipelineViewportStateCreateInfo viewportState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .viewportCount = 1,
        .scissorCount = 1,
    };

    VkPipelineRasterizationStateCreateInfo rasterizer
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .cullMode = VK_CULL_MODE_BACK_BIT,
        .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
        .lineWidth = 1.0f,
    };

    VkPipelineMultisampleStateCreateInfo multisampling
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .rasterizationSamples = m_physicalDevice.msaa(),
        .sampleShadingEnable = VK_FALSE,
    };

    VkPipelineDepthStencilStateCreateInfo depthStencil
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = VK_TRUE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_GREATER,
        .depthBoundsTestEnable = VK_FALSE,
        .stencilTestEnable = VK_FALSE,
    };

    VkPipelineColorBlendAttachmentState colorBlendAttachment
    {
        .blendEnable = VK_FALSE,
        .colorWriteMask =
        VK_COLOR_COMPONENT_R_BIT |
        VK_COLOR_COMPONENT_G_BIT |
        VK_COLOR_COMPONENT_B_BIT |
        VK_COLOR_COMPONENT_A_BIT,
    };

    VkPipelineColorBlendStateCreateInfo colorBlending
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .logicOpEnable = VK_FALSE,
        .logicOp = VK_LOGIC_OP_COPY,
        .attachmentCount = 1,
        .pAttachments = &colorBlendAttachment,
        .blendConstants = {0.0f, 0.0f, 0.0f, 0.0f},
    };

    std::vector<VkDynamicState> dynamicStates =
    {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR
    };

    VkPipelineDynamicStateCreateInfo dynamicState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .dynamicStateCount = static_cast<uint32_t>( dynamicStates.size() ),
        .pDynamicStates = dynamicStates.data(),
    };

    VkDescriptorSetLayoutBinding descriptorsetLayoutBinding
    {
        .binding = 0,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        .pImmutableSamplers = nullptr,
    };

    VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCI
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .bindingCount = 1,
        .pBindings = &descriptorsetLayoutBinding,
    };
    VK_CHECK_RESULT( vkCreateDescriptorSetLayout( m_device(), &descriptorSetLayoutCI, nullptr, &m_descriptorSetLayout ) );

    VkPipelineLayoutCreateInfo pipelineLayoutInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .setLayoutCount = 1,
        .pSetLayouts = &m_descriptorSetLayout,
    };
    VK_CHECK_RESULT( vkCreatePipelineLayout( m_device(),
                     &pipelineLayoutInfo,
                     nullptr,
                     &m_pipelineLayoutRender ) );

    VkGraphicsPipelineCreateInfo pipelineInfo
    {
        .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .stageCount = 2,
        .pStages = shaderStages,
        .pVertexInputState = &vertexInputInfo,
        .pInputAssemblyState = &inputAssembly,
        .pViewportState = &viewportState,
        .pRasterizationState = &rasterizer,
        .pMultisampleState = &multisampling,
        .pDepthStencilState = &depthStencil,
        .pColorBlendState = &colorBlending,
        .pDynamicState = &dynamicState,
        .layout = m_pipelineLayoutRender,
        .renderPass = m_renderPass.renderPassSwapchain(),
    };
    VK_CHECK_RESULT( vkCreateGraphicsPipelines( m_device(),
                     VK_NULL_HANDLE,
                     1,
                     &pipelineInfo,
                     nullptr,
                     &m_pipelineRender ) );

    vkDestroyShaderModule( m_device(), fragShaderModule, nullptr );
    vkDestroyShaderModule( m_device(), vertShaderModule, nullptr );
}


/**************************************************************************************************/
void App::createVertexBuffers()
{
    float vertices[32] = { -1, -1, 1, 1,
                           1, -1, 1, 1,
                           -1, -1, -1, 1,
                           1, -1, -1, 1,
                           -1, 1, 1, 1,
                           1, 1, 1, 1,
                           -1, 1, -1, 1,
                           1, 1, -1, 1,
                         };

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingMemory;
    VkDeviceSize stagingSize = 8 * 4 * sizeof( float );

    m_utils.createBuffer( stagingSize,
                          VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                          VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                          vertices,
                          &stagingBuffer,
                          &stagingMemory );

    m_utils.createBuffer( stagingSize,
                          VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                          nullptr,
                          &m_bufferVertex,
                          &m_memoryVertex );

    m_utils.copyBuffer( stagingBuffer, m_bufferVertex, stagingSize );

    m_utils.destroyBuffer( &stagingBuffer, &stagingMemory );

    vertices[0] = -2;
    vertices[1] = -2;
    vertices[2] = 2;

    m_utils.createBuffer( stagingSize,
                          VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                          VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                          vertices,
                          &stagingBuffer,
                          &stagingMemory );

    m_utils.createBuffer( stagingSize,
                          VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                          nullptr,
                          &m_bufferVertex2,
                          &m_memoryVertex2 );

    m_utils.copyBuffer( stagingBuffer, m_bufferVertex2, stagingSize );

    m_utils.destroyBuffer( &stagingBuffer, &stagingMemory );

}

/**************************************************************************************************/
void App::createIndexBuffers()
{
    uint16_t indices[12] =
    {
        0, 1, 2,
        2, 1, 3,
        4, 5, 6,
        6, 5, 7
    };

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingMemory;
    VkDeviceSize stagingSize = 4 * 3 * sizeof( uint16_t );

    m_utils.createBuffer( stagingSize,
                          VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                          VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                          &indices,
                          &stagingBuffer,
                          &stagingMemory );

    m_utils.createBuffer( stagingSize,
                          VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                          nullptr,
                          &m_bufferIndex,
                          &m_memoryIndex );

    m_utils.copyBuffer( stagingBuffer, m_bufferIndex, stagingSize );

    m_utils.destroyBuffer( &stagingBuffer, &stagingMemory );
}

/**************************************************************************************************/
void App::createUBOBuffers()
{
    UBOVS uboVS
    {
        .projection = glm::perspective( glm::radians( 45.0f ), 1.0f, 1000.0f, 0.01f ),
        .model = glm::lookAt( glm::vec3( 2.0f, 10.0f, 4.0f ),
                              glm::vec3( 0.0f, 0.0f, 0.0f ),
                              glm::vec3( 0.0f, 1.0f, 0.0f ) )
    };

    uboVS.projection[1][1] *= -1.0;

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingMemory;
    VkDeviceSize stagingSize = sizeof( uboVS );

    m_utils.createBuffer( stagingSize,
                          VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                          VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                          &uboVS,
                          &stagingBuffer,
                          &stagingMemory );

    m_utils.createBuffer( stagingSize,
                          VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                          nullptr,
                          &m_bufferUBO,
                          &m_memoryUBO );

    m_utils.copyBuffer( stagingBuffer, m_bufferUBO, stagingSize );

    m_utils.destroyBuffer( &stagingBuffer, &stagingMemory );
}

/**************************************************************************************************/
void App::prepareBuffers()
{
    createVertexBuffers();
    createIndexBuffers();
    createUBOBuffers();

    std::array<VkDescriptorPoolSize, 1> poolSizes
    {
        VkDescriptorPoolSize{
            .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = 1,
        }
    };

    VkDescriptorPoolCreateInfo poolInfo
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .maxSets = static_cast<uint32_t>( 1 ),
        .poolSizeCount = static_cast<uint32_t>( poolSizes.size() ),
        .pPoolSizes = poolSizes.data(),
    };

    VK_CHECK_RESULT( vkCreateDescriptorPool( m_device(), &poolInfo, nullptr, &m_descriptorPool ) );

    VkDescriptorSetAllocateInfo allocInfo
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .descriptorPool = m_descriptorPool,
        .descriptorSetCount = 1,
        .pSetLayouts = &m_descriptorSetLayout,
    };

    VK_CHECK_RESULT( vkAllocateDescriptorSets( m_device(), &allocInfo, &m_descriptorSet ) );

    VkDescriptorBufferInfo bufferInfo
    {
        .buffer = m_bufferUBO,
        .offset = 0,
        .range = sizeof( UBOVS ),
    };

    std::array<VkWriteDescriptorSet, 1> descriptorWrites
    {
        VkWriteDescriptorSet{

            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = m_descriptorSet,
            .dstBinding = 0,
            .dstArrayElement = 0,
            .descriptorCount = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .pBufferInfo = &bufferInfo,
        }};

    vkUpdateDescriptorSets( m_device(),
                            static_cast<uint32_t>( descriptorWrites.size() ),
                            descriptorWrites.data(),
                            0,
                            nullptr );
}

/**************************************************************************************************/
void App::fillCommandBufferPreRenderPass()
{
}

/**************************************************************************************************/
void App::fillCommandBuffer()
{
    static bool toggle = false;

    vkCmdBindPipeline( m_commandBuffers[m_currentFrame],
                       VK_PIPELINE_BIND_POINT_GRAPHICS,
                       m_pipelineRender );

    vkCmdBindIndexBuffer( m_commandBuffers[m_currentFrame],
                          m_bufferIndex,
                          0,
                          VK_INDEX_TYPE_UINT16 );

    VkDeviceSize offsets[1] = {};
    vkCmdBindVertexBuffers( m_commandBuffers[m_currentFrame],
                            0,
                            1,
                            &( toggle ? m_bufferVertex : m_bufferVertex2 ),
                            offsets );

    vkCmdBindDescriptorSets( m_commandBuffers[m_currentFrame],
                             VK_PIPELINE_BIND_POINT_GRAPHICS,
                             m_pipelineLayoutRender,
                             0,
                             1,
                             &m_descriptorSet,
                             0,
                             NULL );

    vkCmdDrawIndexed( m_commandBuffers[m_currentFrame],
                      12,
                      1,
                      0,
                      0,
                      0 );
    toggle = !toggle;
}


/**************************************************************************************************/
int main( int, char *[] )
{
    app = new App();
    app->create();

    int i = 0;

    while( app->m_window.pollWindow() )
    {
        if( app->acquireNextFrame() )
        {
            app->renderNextFrame();
            app->presentNextFrame();
        }
    }

    app->destroy();
    delete( app );
    app = nullptr;

    std::cout << "Done" << std::endl;
    return 0;
}
