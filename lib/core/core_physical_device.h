#pragma once
#include <vulkan/vulkan.h>

class PhysicalDevice
{
public:
    PhysicalDevice();
    virtual ~PhysicalDevice();

    VkPhysicalDevice operator()()
    {
        return m_physicalDevice;
    }

    void select(VkInstance instance, bool MSAA );

    VkPhysicalDeviceMemoryProperties memoryProperties() const;
    VkSampleCountFlagBits msaa() const;
    uint32_t queueFamilyGraphics() const;
    uint32_t queueFamilyTransfer() const;
    uint32_t queueFamilyCompute() const;

private:
    void getMemoryProperties();
    void getQueueFamilies();
    void getFormats();

    VkInstance m_instance = VK_NULL_HANDLE;

    VkPhysicalDevice m_physicalDevice = VK_NULL_HANDLE;

    VkPhysicalDeviceMemoryProperties m_memoryProperties{};
    VkSampleCountFlagBits m_msaa = VK_SAMPLE_COUNT_8_BIT;
    uint32_t m_queueFamilyGraphics = UINT32_MAX;
    uint32_t m_queueFamilyTransfer = UINT32_MAX;
    uint32_t m_queueFamilyCompute = UINT32_MAX;

};
