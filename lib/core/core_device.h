#pragma once
#include <vector>
#include <string>
#include <vulkan/vulkan.h>

class Device
{
public:
    Device();
    virtual ~Device();

    VkDevice operator()()
    {
        return m_device;
    }

    void create(VkPhysicalDevice physicalDevice,
                uint32_t queueFamilyGraphics,
                bool swapchain);
    void destroy();

    VkQueue queueGraphics() const;

private:
    void initRequestedDeviceLayers();
    void initRequestedDeviceExtensions(bool swapchain);
    void initRequestedDeviceFeatures();

    VkPhysicalDevice m_physicalDevice = VK_NULL_HANDLE;
    VkDevice m_device = VK_NULL_HANDLE;

    std::vector<std::string> m_deviceLayerNamesRequested;
    std::vector<std::string> m_deviceExtensionNamesRequested;
    VkPhysicalDeviceFeatures m_features{};
    void *m_deviceCreateInfoChain = nullptr;
    VkQueue m_queueGraphics = VK_NULL_HANDLE;
};
