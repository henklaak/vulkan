#pragma once
#include <vulkan/vulkan.h>

#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

class WindowSdl2;

class SdlSurface
{
public:
    SdlSurface();
    virtual ~SdlSurface();

    VkSurfaceKHR operator()()
    {
        return m_surface;
    }

    void create(VkInstance instance,
                VkPhysicalDevice physicalDevice,
                WindowSdl2 *window,
                bool VSYNC);
    void destroy();

    VkSurfaceFormatKHR surfaceFormat();
    VkExtent2D extent();
    uint32_t minImageCount();
    VkSurfaceTransformFlagBitsKHR currentTransform();
    VkPresentModeKHR presentMode() const;

private:
    VkInstance m_instance = VK_NULL_HANDLE;
    VkPhysicalDevice m_physicalDevice = VK_NULL_HANDLE;
    VkSurfaceKHR m_surface = VK_NULL_HANDLE;
    VkSurfaceCapabilitiesKHR m_surfaceCapabilities{};
    VkSurfaceFormatKHR m_surfaceFormat{};
    VkPresentModeKHR m_presentModeSurface = VK_PRESENT_MODE_MAX_ENUM_KHR;

};
