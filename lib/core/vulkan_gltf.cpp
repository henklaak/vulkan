#include "sdl_basis.h"

/**************************************************************************************************/
void VulkanSdl::loadGltfModel( const std::string &filename,
                               tinygltf::Model *model )
{
    tinygltf::TinyGLTF context{};
    std::string err;
    std::string warn;

    assert( context.LoadBinaryFromFile( model, &err, &warn, filename ) );
    prepareGltfModel( *model );
}

/**************************************************************************************************/
void VulkanSdl::prepareGltfModel( const tinygltf::Model &model )
{
    for( const tinygltf::Buffer &buffer : model.buffers )
    {
        VkBuffer stagingBuffer;
        VkDeviceMemory stagingMemory;
        VkDeviceSize stagingSize = buffer.data.size();

        m_utils.createBuffer( stagingSize,
                              VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                              VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                              buffer.data.data(),
                              &stagingBuffer,
                              &stagingMemory );

        m_utils.createBuffer( stagingSize,
                              VK_BUFFER_USAGE_TRANSFER_DST_BIT |
                              VK_BUFFER_USAGE_INDEX_BUFFER_BIT |
                              VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                              VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                              nullptr,
                              &m_bufferGltf,
                              &m_memoryGltf );
        m_utils.copyBuffer( stagingBuffer, m_bufferGltf, stagingSize );

        m_utils.destroyBuffer( &stagingBuffer, &stagingMemory );
    }
}
