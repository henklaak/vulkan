#include "core_utils.h"
#include <cstring>
#include <fstream>
#include <stdarg.h>
#include "core_log.h"

/**************************************************************************************************/
Utils::Utils()
{}

/**************************************************************************************************/
Utils::~Utils()
{}

/**************************************************************************************************/
void Utils::create( VkInstance instance,
                    VkPhysicalDevice physicalDevice,
                    VkPhysicalDeviceMemoryProperties memoryProperties,
                    uint32_t queueFamily,
                    VkDevice device,
                    VkQueue queue )
{
    m_instance = instance;
    m_physicalDevice = physicalDevice;
    m_memoryProperties = memoryProperties;
    m_queueFamily = queueFamily;
    m_device = device;
    m_queue = queue;

    VkCommandPoolCreateInfo cmdPoolInfo =
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
        .queueFamilyIndex = queueFamily
    };
    LOG_DEBUG( "vkCreateCommandPool()" );
    VK_CHECK_RESULT( vkCreateCommandPool( m_device, &cmdPoolInfo, nullptr, &m_commandPool ) );

    vkCmdBeginDebugUtilsLabelEXT = reinterpret_cast<PFN_vkCmdBeginDebugUtilsLabelEXT>(
                                       vkGetInstanceProcAddr( instance, "vkCmdBeginDebugUtilsLabelEXT" ) );
    vkCmdEndDebugUtilsLabelEXT = reinterpret_cast<PFN_vkCmdEndDebugUtilsLabelEXT>(
                                     vkGetInstanceProcAddr( instance, "vkCmdEndDebugUtilsLabelEXT" ) );
    vkSetDebugUtilsObjectNameEXT = reinterpret_cast<PFN_vkSetDebugUtilsObjectNameEXT>(
                                       vkGetInstanceProcAddr( instance, "vkSetDebugUtilsObjectNameEXT" ) );

    setObjectName( uint64_t( m_device ), VK_OBJECT_TYPE_DEVICE, "Device main" );
}

/**************************************************************************************************/
void Utils::destroy()
{
    vkDestroyCommandPool( m_device, m_commandPool, nullptr );
}

/**************************************************************************************************/
void Utils::beginLabelRegion( VkCommandBuffer cmdbuffer,
                              const char *pMarkerName,
                              glm::vec4 color )
{
    // Check for valid function pointer (may not be present if not running in a debugging application)
    VkDebugUtilsLabelEXT markerInfo =
    {
        .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_LABEL_EXT,
        .pLabelName = pMarkerName
    };
    memcpy( markerInfo.color, &color[0], sizeof( float ) * 4 );
    vkCmdBeginDebugUtilsLabelEXT( cmdbuffer, &markerInfo );
}

/**************************************************************************************************/
void Utils::endLabelRegion( VkCommandBuffer cmdBuffer )
{
    vkCmdEndDebugUtilsLabelEXT( cmdBuffer );
}

/**************************************************************************************************/
void Utils::setObjectName( uint64_t object,
                           VkObjectType objectType,
                           const char *txt, ... )
{
    if( m_device )
    {
        va_list ap;
        va_start( ap, txt );
        char buf[256];
        vsnprintf( buf, 256, txt, ap );
        va_end( ap );

        VkDebugUtilsObjectNameInfoEXT nameInfo =
        {
            .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT,
            .objectType = objectType,
            .objectHandle = object,
            .pObjectName = buf
        };
        VK_CHECK_RESULT( vkSetDebugUtilsObjectNameEXT( m_device, &nameInfo ) );
    }
}


/**************************************************************************************************/
VkCommandBuffer Utils::beginSingleTimeCommands()
{
    VkCommandBufferAllocateInfo allocInfo
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .commandPool = m_commandPool,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1
    };

    VkCommandBuffer tmpCommandBuffer;
    LOG_DEBUG( "vkAllocateCommandBuffers()" );
    VK_CHECK_RESULT( vkAllocateCommandBuffers( m_device, &allocInfo, &tmpCommandBuffer ) );

    VkCommandBufferBeginInfo beginInfo
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
    };

    vkBeginCommandBuffer( tmpCommandBuffer, &beginInfo );

    return tmpCommandBuffer;
}

/**************************************************************************************************/
void Utils::endSingleTimeCommands( VkCommandBuffer tmpCommandBuffer )
{
    VK_CHECK_RESULT( vkEndCommandBuffer( tmpCommandBuffer ) );

    VkSubmitInfo submitInfo
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .commandBufferCount = 1,
        .pCommandBuffers = &tmpCommandBuffer
    };

    vkQueueSubmit( m_queue, 1, &submitInfo, VK_NULL_HANDLE );
    vkQueueWaitIdle( m_queue );

    LOG_DEBUG( "vkFreeCommandBuffers()" );
    vkFreeCommandBuffers( m_device, m_commandPool, 1, &tmpCommandBuffer );
}

/**************************************************************/
void Utils::allocateMemory( VkMemoryPropertyFlags properties,
                            VkMemoryRequirements memoryRequirements,
                            VkDeviceMemory *memory )
{
    VkMemoryAllocateInfo allocationInfo
    {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .allocationSize = memoryRequirements.size
    };

    uint32_t memoryTypeIndex = 0;
    auto memoryProperties = m_memoryProperties;

    for( uint32_t i = 0; i < memoryProperties.memoryTypeCount; i++ )
    {
        if( ( memoryRequirements.memoryTypeBits & ( 1 << i ) ) &&
                ( memoryProperties.memoryTypes[i].propertyFlags & properties ) ==
                properties )
        {
            memoryTypeIndex = i;
            break;
        }
    }

    allocationInfo.memoryTypeIndex = memoryTypeIndex;

    LOG_DEBUG( "vkAllocateMemory()" );
    VK_CHECK_RESULT( vkAllocateMemory( m_device, &allocationInfo, nullptr, memory ) );
}

/**************************************************************************************************/
void Utils::freeMemory( VkDeviceMemory *memory )
{
    LOG_DEBUG( "vkFreeMemory()" );
    vkFreeMemory( m_device, *memory, nullptr );
    *memory = VK_NULL_HANDLE;
}

/**************************************************************************************************/
void Utils::createBuffer( VkDeviceSize size,
                          VkBufferUsageFlags usage,
                          VkMemoryPropertyFlags properties,
                          const void *initialData,
                          VkBuffer *buffer,
                          VkDeviceMemory *bufferMemory )
{
    VkBufferCreateInfo bufferCI
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = size,
        .usage = usage,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices = &m_queueFamily
    };

    LOG_DEBUG( "vkCreateBuffer()" );
    VK_CHECK_RESULT( vkCreateBuffer( m_device, &bufferCI, nullptr, buffer ) );

    VkMemoryRequirements memoryRequirements{};
    vkGetBufferMemoryRequirements( m_device, *buffer, &memoryRequirements );

    allocateMemory( properties, memoryRequirements, bufferMemory );

    if( initialData )
    {
        void *data = nullptr;
        mapBuffer( *bufferMemory, &data );
        memcpy( data, initialData, size );
        flushBuffer( *bufferMemory );
        unmapBuffer( *bufferMemory, &data );
    }

    VK_CHECK_RESULT( vkBindBufferMemory( m_device, *buffer, *bufferMemory, 0 ) );
}

/**************************************************************************************************/
void Utils::destroyBuffer( VkBuffer *buffer,
                           VkDeviceMemory *bufferMemory )
{
    if( bufferMemory )
    {
        LOG_DEBUG( "vkFreeMemory()" );
        vkFreeMemory( m_device, *bufferMemory, nullptr );
        *bufferMemory = VK_NULL_HANDLE;
    }

    if( buffer )
    {
        LOG_DEBUG( "vkDestroyBuffer()" );
        vkDestroyBuffer( m_device, *buffer, nullptr );
        *buffer = VK_NULL_HANDLE;
    }
}

/**************************************************************************************************/
void Utils::mapBuffer( VkDeviceMemory memory, void **mapped )
{
    VK_CHECK_RESULT( vkMapMemory( m_device, memory, 0, VK_WHOLE_SIZE, 0, mapped ) );
}

/**************************************************************************************************/
void Utils::unmapBuffer( VkDeviceMemory memory, void **mapped )
{
    vkUnmapMemory( m_device, memory );
    *mapped = nullptr;
}

/**************************************************************************************************/
void Utils::copyBuffer( VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size )
{
    VkCommandBuffer cmdBuffer = beginSingleTimeCommands();

    VkBufferCopy copyRegion
    {
        .srcOffset = 0,
        .dstOffset = 0,
        .size = size,
    };
    vkCmdCopyBuffer( cmdBuffer, srcBuffer, dstBuffer, 1, &copyRegion );

    endSingleTimeCommands( cmdBuffer );
}

/**************************************************************************************************/
void Utils::flushBuffer( VkDeviceMemory memory )
{
    VkMappedMemoryRange mappedRange =
    {
        .sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
        .memory = memory,
        .offset = 0,
        .size = VK_WHOLE_SIZE
    };
    VK_CHECK_RESULT( vkFlushMappedMemoryRanges( m_device, 1, &mappedRange ) );
}

/**************************************************************************************************/
void Utils::invalidateBuffer( VkDeviceMemory memory )
{
    VkMappedMemoryRange mappedRange =
    {
        .sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
        .memory = memory,
        .offset = 0,
        .size = VK_WHOLE_SIZE
    };
    VK_CHECK_RESULT( vkInvalidateMappedMemoryRanges( m_device, 1, &mappedRange ) );
}

/**************************************************************************************************/
void Utils::createImage( VkFormat format,
                         VkExtent2D extent,
                         uint32_t mipLevels,
                         uint32_t arrayLayers,
                         VkSampleCountFlagBits samples,
                         VkImageTiling imageTiling,
                         VkImageUsageFlags usage,
                         VkMemoryPropertyFlags properties,
                         VkImage *image,
                         VkDeviceMemory *imageMemory )
{
    VkImageCreateInfo imageCI
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = format,
        .extent {
            .width = extent.width,
            .height = extent.height,
            .depth = 1 },
        .mipLevels = mipLevels,
        .arrayLayers = arrayLayers,
        .samples = samples,
        .tiling = imageTiling,
        .usage = usage,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices = &m_queueFamily,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
    };

    LOG_DEBUG( "vkCreateImage" );
    VK_CHECK_RESULT( vkCreateImage( m_device, &imageCI, nullptr, image ) );

    // Create memory for the image
    VkMemoryRequirements memoryRequirements;
    vkGetImageMemoryRequirements( m_device, *image, &memoryRequirements );

    allocateMemory( properties, memoryRequirements, imageMemory );

    VK_CHECK_RESULT( vkBindImageMemory( m_device, *image, *imageMemory, 0 ) );
}

/**************************************************************************************************/
void Utils::destroyImage( VkImage *image, VkDeviceMemory *imageMemory )
{
    LOG_DEBUG( "vkFreeMemory()" );
    vkFreeMemory( m_device, *imageMemory, nullptr );
    *imageMemory = VK_NULL_HANDLE;

    LOG_DEBUG( "vkDestroyImage()" );
    vkDestroyImage( m_device, *image, nullptr );
    *image = VK_NULL_HANDLE;
}

/**************************************************************************************************/
void Utils::createImageView( VkImage image,
                             VkImageViewType viewType,
                             VkFormat format,
                             VkImageAspectFlagBits aspectFlagBits,
                             uint32_t mipLevels,
                             uint32_t arrayLayers,
                             VkImageView *imageView,
                             void *pNext )
{
    VkImageViewCreateInfo imageViewCI
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .pNext = pNext,
        .flags = 0,
        .image = image,
        .viewType = viewType,
        .format = format,
        .components {
            .r = VK_COMPONENT_SWIZZLE_IDENTITY,
            .g = VK_COMPONENT_SWIZZLE_IDENTITY,
            .b = VK_COMPONENT_SWIZZLE_IDENTITY,
            .a = VK_COMPONENT_SWIZZLE_IDENTITY},
        .subresourceRange {
            .aspectMask = aspectFlagBits,
            .baseMipLevel = 0,
            .levelCount = mipLevels,
            .baseArrayLayer = 0,
            .layerCount = arrayLayers
        }
    };

    LOG_DEBUG( "vkCreateImageView()" );
    VK_CHECK_RESULT( vkCreateImageView( m_device, &imageViewCI, nullptr, imageView ) );
}

/**************************************************************************************************/
void Utils::destroyImageView( VkImageView *imageView )
{
    LOG_DEBUG( "vkDestroyImageView()" );
    vkDestroyImageView( m_device, *imageView, nullptr );
    *imageView = VK_NULL_HANDLE;
}

/**************************************************************************************************/
void Utils::transitionImageLayout( VkCommandBuffer cmd,
                                   VkImage image,
                                   VkImageLayout srcLayout,
                                   VkImageLayout dstLayout )
{
    VkImageMemoryBarrier barrier
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .oldLayout = srcLayout,
        .newLayout = dstLayout,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = image,
        .subresourceRange {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1}
    };


    VkPipelineStageFlags sourceStage = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    barrier.srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT;

    VkPipelineStageFlags destinationStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;

    vkCmdPipelineBarrier( cmd,
                          sourceStage, destinationStage, 0,
                          0, nullptr,
                          0, nullptr,
                          1, &barrier );
}

/**************************************************************************************************/
void Utils::copyBufferToImage( VkCommandBuffer commandBuffer,
                               VkBuffer buffer,
                               VkImage image,
                               uint32_t width,
                               uint32_t height )
{
    VkBufferImageCopy region
    {
        .bufferOffset = 0,
        .bufferRowLength = 0,
        .bufferImageHeight = 0,
        .imageSubresource{
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = 0,
            .baseArrayLayer = 0,
            .layerCount = 1},
        .imageOffset = {0, 0, 0},
        .imageExtent =
        {
            width,
            height,
            1
        }
    };

    vkCmdCopyBufferToImage( commandBuffer,
                            buffer,
                            image,
                            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                            1,
                            &region );
}

/**************************************************************************************************/
VkShaderModule Utils::createShaderModule( const std::vector<char> &code )
{
    VkShaderModuleCreateInfo createInfo
    {
        .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .codeSize = code.size(),
        .pCode = reinterpret_cast<const uint32_t *>( code.data() ),
    };

    VkShaderModule shaderModule;

    if( vkCreateShaderModule( m_device, &createInfo, nullptr, &shaderModule ) != VK_SUCCESS )
    {
        throw std::runtime_error( "failed to create shader module!" );
    }

    return shaderModule;
}

/**************************************************************************************************/
std::vector<char> Utils::readFile( const std::string &filename )
{
    std::ifstream file( filename, std::ios::ate | std::ios::binary );

    if( !file.is_open() )
    {
        throw std::runtime_error( "failed to open file!" );
    }

    size_t fileSize = ( size_t ) file.tellg();
    std::vector<char> buffer( fileSize );

    file.seekg( 0 );
    file.read( buffer.data(), fileSize );

    file.close();

    return buffer;
}


/**************************************************************************************************/
void Utils::loadGltfModel( const std::string &filename,
                           tinygltf::Model *model )
{
    tinygltf::TinyGLTF context{};
    std::string err;
    std::string warn;

    assert( context.LoadBinaryFromFile( model, &err, &warn, filename ) );
}
