#pragma once
#include <array>
#include <iostream>
#include <stdexcept>
#include <vector>

#include <vulkan/vulkan.h>
#include <glm/glm.hpp>

#include <tiny_gltf.h>

#include "sdl_window.h"
#include "core_instance.h"
#include "core_messenger.h"
#include "core_physical_device.h"
#include "sdl_surface.h"
#include "core_device.h"
#include "core_utils.h"
#include "core_commandpool.h"
#include "sdl_commandbuffers.h"
#include "core_renderpasses.h"
#include "sdl_swapchain.h"
#include "core_framebuffers.h"
#include "sdl_sync.h"

const int MAX_FRAMES_IN_FLIGHT = 2;

class VulkanSdl
{
public:

    bool MSAA  = true;
    bool FXAA  = false;
    bool DEBUG = true;
    bool VSYNC = true;
    bool SWAPCHAIN = true;


    explicit VulkanSdl(const std::string &windowTitle);
    virtual ~VulkanSdl();

    // vulkan_core

    virtual void create() ;
    virtual void destroy();

    virtual void prepare() = 0;
    virtual void fillCommandBufferPreRenderPass() = 0;
    virtual void fillCommandBuffer() = 0;
    virtual void unprepare() = 0;

    // vulkan_window
    WindowSdl2 m_window;
    bool pollWindow()
    {
        return m_window.pollWindow();
    }

    Instance m_instance;
    Messenger m_messenger;
    PhysicalDevice m_physicalDevice;
    SdlSurface m_surface;
    Device m_device;
    Utils m_utils;
    CommandPool m_commandPool;
    CommandBuffers m_commandBuffers;
    RenderPass m_renderPass;
    Swapchain m_swapchain;
    Framebuffers m_framebuffers;
    Sync m_sync;

    // vulkan_sync
    void createSyncObjects();
    void destroySyncObjects();
    uint32_t m_currentFrame = 0;

    // vulkan_render
    bool acquireNextFrame();
    void renderNextFrame();
    void presentNextFrame();
    uint32_t m_swapchainAcquiredIndex = 0;

    void loadGltfModel( const std::string &filename,
                        tinygltf::Model  *model );
    void prepareGltfModel( const tinygltf::Model &model );
    VkBuffer m_bufferGltf;
    VkDeviceMemory m_memoryGltf;

private:
    std::string m_windowTitle;

};

