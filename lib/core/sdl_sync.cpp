#include "sdl_sync.h"
#include "core_log.h"

/**************************************************************************************************/
Sync::Sync()
{}

/**************************************************************************************************/
Sync::~Sync()
{}

/**************************************************************************************************/
void Sync::create( VkDevice device,
                   uint32_t nrFrames )
{
    m_device = device;
    m_nrFrames = nrFrames;

    m_fenceInFlight.resize( m_nrFrames );
    m_semaphoreImageAvailable.resize( m_nrFrames );
    m_semaphoreRenderFinished.resize( m_nrFrames );

    for( size_t i = 0; i < m_nrFrames; ++i )
    {
        VkFenceCreateInfo fenceCI
        {
            .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
            .flags = VK_FENCE_CREATE_SIGNALED_BIT
        };

        LOG_DEBUG( "vkCreateFence()" );
        VK_CHECK_RESULT( vkCreateFence( m_device, &fenceCI, nullptr, &m_fenceInFlight[i] ) );

        VkSemaphoreCreateInfo semaphoreCI
        {
            .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO
        };

        LOG_DEBUG( "vkCreateSemaphore()" );
        VK_CHECK_RESULT( vkCreateSemaphore( m_device, &semaphoreCI, nullptr, &m_semaphoreImageAvailable[i] ) );

        LOG_DEBUG( "vkCreateSemaphore()" );
        VK_CHECK_RESULT( vkCreateSemaphore( m_device, &semaphoreCI, nullptr, &m_semaphoreRenderFinished[i] ) );
    }
}

/**************************************************************************************************/
void Sync::destroy()
{
    for( size_t i = 0; i < m_nrFrames; ++i )
    {
        if( m_semaphoreRenderFinished[i] )
        {
            LOG_DEBUG( "vkDestroyRenderPass()" );
            vkDestroySemaphore( m_device, m_semaphoreRenderFinished[i], nullptr );
            m_semaphoreRenderFinished[i] = VK_NULL_HANDLE;
        }

        if( m_semaphoreImageAvailable[i] )
        {
            LOG_DEBUG( "vkDestroySemaphore()" );
            vkDestroySemaphore( m_device, m_semaphoreImageAvailable[i], nullptr );
            m_semaphoreImageAvailable[i] = VK_NULL_HANDLE;
        }

        if( m_fenceInFlight[i] )
        {
            LOG_DEBUG( "vkDestroyFence()" );
            vkDestroyFence( m_device, m_fenceInFlight[i], nullptr );
            m_fenceInFlight[i] = VK_NULL_HANDLE;
        }
    }
}

