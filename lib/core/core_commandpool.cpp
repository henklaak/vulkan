#include "core_commandpool.h"
#include "core_log.h"

/**************************************************************************************************/
CommandPool::CommandPool()
{}

/**************************************************************************************************/
CommandPool::~CommandPool()
{}

/**************************************************************************************************/
void CommandPool::create( VkPhysicalDevice physicalDevice,
                          VkDevice device,
                          uint32_t queueFamilyGraphics )
{
    m_device = device;
    VkCommandPoolCreateInfo cmdPoolInfo =
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
        .queueFamilyIndex = queueFamilyGraphics
    };
    LOG_DEBUG( "vkCreateCommandPool()" );
    VK_CHECK_RESULT( vkCreateCommandPool( m_device, &cmdPoolInfo, nullptr, &m_commandPoolGraphics ) );
    //m_debug.setObjectName( uint64_t( m_commandPoolGraphics ), VK_OBJECT_TYPE_COMMAND_POOL, "CommandPool Graphics" );
}

/**************************************************************************************************/
void CommandPool::destroy()
{
    if( m_commandPoolGraphics )
    {
        LOG_DEBUG( "vkDestroyCommandPool()" );
        vkDestroyCommandPool( m_device, m_commandPoolGraphics, nullptr );
        m_commandPoolGraphics = VK_NULL_HANDLE;
    }
}


