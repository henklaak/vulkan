#pragma once
#include <vulkan/vulkan.h>
#include <vector>

class Sync
{
public:
    Sync();
    virtual ~Sync();

    void create(VkDevice device,
                uint32_t nrFrames);
    void destroy();

    VkFence fenceInFlight(int i)
    {
        return m_fenceInFlight[i];
    }

    VkSemaphore semaphoreImageAvailable(int i)
    {
        return m_semaphoreImageAvailable[i];
    }

    VkSemaphore semaphoreRenderFinished(int i)
    {
        return m_semaphoreRenderFinished[i];
    }

private:
    VkDevice m_device=VK_NULL_HANDLE;
    uint32_t m_nrFrames=0;

    std::vector<VkFence> m_fenceInFlight;
    std::vector<VkSemaphore> m_semaphoreImageAvailable;
    std::vector<VkSemaphore> m_semaphoreRenderFinished;

};
