#pragma once
#include <vector>
#include <vulkan/vulkan.h>

class CommandBuffers
{
public:
    CommandBuffers();
    virtual ~CommandBuffers();

    VkCommandBuffer operator[](int i)
    {
        return m_commandBuffers[i];
    }

    void create(VkDevice device,
                VkCommandPool commandPool,
                uint32_t nrBuffers);
    void destroy();

    void begin(uint32_t currentFrame, VkExtent2D extent);
    void end();

private:
    VkDevice m_device = VK_NULL_HANDLE;
    VkCommandPool m_commandPool = VK_NULL_HANDLE;

    std::vector<VkCommandBuffer> m_commandBuffers;
    uint32_t m_currentFrame=0;
};
