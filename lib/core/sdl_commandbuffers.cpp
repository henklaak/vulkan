#include "sdl_commandbuffers.h"
#include "core_log.h"

/**************************************************************************************************/
CommandBuffers::CommandBuffers()
{}

/**************************************************************************************************/
CommandBuffers::~CommandBuffers()
{}

/**************************************************************************************************/
void CommandBuffers::create( VkDevice device,
                             VkCommandPool commandPool,
                             uint32_t nrBuffers )
{
    m_device = device;
    m_commandPool = commandPool;

    m_commandBuffers.resize( nrBuffers );

    VkCommandBufferAllocateInfo cmdAllocInfo
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext = nullptr,
        .commandPool = commandPool,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = static_cast<uint32_t>( m_commandBuffers.size() )
    };
    LOG_DEBUG( "vkAllocateCommandBuffers()" );
    VK_CHECK_RESULT( vkAllocateCommandBuffers( m_device, &cmdAllocInfo, m_commandBuffers.data() ) );

    for( size_t i = 0; i < m_commandBuffers.size(); ++i )
    {
//        m_debug.setObjectName( uint64_t( m_commandBuffersGraphics[i] ),
//                               VK_OBJECT_TYPE_COMMAND_BUFFER,
//                               "CommandBuffer %d", i );
    }
}

/**************************************************************************************************/
void CommandBuffers::destroy()
{
    LOG_DEBUG( "vkFreeCommandBuffers()" );
    vkFreeCommandBuffers( m_device, m_commandPool,
                          m_commandBuffers.size(),
                          m_commandBuffers.data() );
}

/**************************************************************************************************/
void CommandBuffers::begin( uint32_t currentFrame, VkExtent2D extent )
{
    m_currentFrame = currentFrame;

    VK_CHECK_RESULT( vkResetCommandBuffer( m_commandBuffers[m_currentFrame], 0 ) );

    VkCommandBufferBeginInfo cmdBeginInfo
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO
    };
    VK_CHECK_RESULT( vkBeginCommandBuffer( m_commandBuffers[m_currentFrame], &cmdBeginInfo ) );

    VkViewport viewport
    {
        .x = 0.0f,
        .y = 0.0f,
        .width = static_cast<float>( extent.width ),
        .height = static_cast<float>( extent.height ),
        .minDepth = 0.0f,
        .maxDepth = 1.0f
    };
    vkCmdSetViewport( m_commandBuffers[m_currentFrame], 0, 1, &viewport );

    VkRect2D scissor
    {
        .offset = {0, 0},
        .extent = extent
    };
    vkCmdSetScissor( m_commandBuffers[m_currentFrame], 0, 1, &scissor );
}

/**************************************************************************************************/
void CommandBuffers::end()
{
    VK_CHECK_RESULT( vkEndCommandBuffer( m_commandBuffers[m_currentFrame] ) );
}
