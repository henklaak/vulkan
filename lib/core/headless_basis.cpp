#include "headless_basis.h"

#include <cstring>
#include <fstream>
#include <sstream>
#include <stdarg.h>
#include <sys/time.h>
#include "core_log.h"

/**************************************************************************************************/
VulkanHeadless::VulkanHeadless()
{
}

/**************************************************************************************************/
VulkanHeadless::~VulkanHeadless()
{
}

/**************************************************************************************************/
void VulkanHeadless::create()
{
    VK::reset_log();
    std::vector<std::string> extensions;
    m_instance.create( extensions, true );
    m_messenger.create( m_instance() );
    m_physicalDevice.select( m_instance(), MSAA );
    m_device.create( m_physicalDevice(), m_physicalDevice.queueFamilyGraphics(), false );
    m_utils.create( m_instance(), m_physicalDevice(), m_physicalDevice.memoryProperties(),
                    m_physicalDevice.queueFamilyGraphics(), m_device(), m_device.queueGraphics() );
    m_commandPool.create( m_physicalDevice(), m_device(), m_physicalDevice.queueFamilyGraphics() );
    m_commandBuffers.create( m_device(), m_commandPool(), 1 );

    m_format = VK_FORMAT_B8G8R8A8_SRGB;
    m_renderPass.create( m_device(), m_format, m_physicalDevice.msaa() );

    m_extent.width = 1280;
    m_extent.height = 720;

    m_utils.createImage( m_format, m_extent, 1, 1,
                         VK_SAMPLE_COUNT_1_BIT,
                         VK_IMAGE_TILING_OPTIMAL,
                         VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                         VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                         &m_imageColor,
                         &m_memoryColor );
    m_utils.createImageView( m_imageColor,
                             VK_IMAGE_VIEW_TYPE_2D,
                             m_format,
                             VK_IMAGE_ASPECT_COLOR_BIT,
                             1,
                             1,
                             &m_imageViewColor );

    std::vector<VkImageView> imageViews;
    imageViews.push_back( m_imageViewColor );

    m_framebuffers.recreate( &m_utils, m_device(), m_renderPass.renderPassHeadless(),
                             imageViews,
                             m_format,
                             m_extent,
                             m_physicalDevice.msaa() );

    m_sync.create( m_device(), 1 );

    prepare();
}

/**************************************************************************************************/
void VulkanHeadless::destroy()
{
    vkDeviceWaitIdle( m_device() );

    unprepare();

    m_sync.destroy();
    m_utils.destroyImageView( &m_imageViewColor );
    m_utils.destroyImage( &m_imageColor, &m_memoryColor );

    m_framebuffers.destroy();
    m_renderPass.destroy();
    m_commandBuffers.destroy();
    m_commandPool.destroy();
    m_utils.destroy();
    m_device.destroy();
    m_messenger.destroy();
    m_instance.destroy();
}


/**************************************************************************************************/
bool VulkanHeadless::acquireNextFrame( )
{
    return true;
}

/**************************************************************************************************/
void VulkanHeadless::renderNextFrame( )
{
    VkFence fence = m_sync.fenceInFlight( 0 );
    VK_CHECK_RESULT( vkResetFences( m_device(), 1, &fence ) );

    m_commandBuffers.begin( 0, m_extent );

    fillCommandBufferPreRenderPass();

    std::vector<VkClearValue> clearValues
    {
        VkClearValue{.color = {{0.1f, 0.1f, 0.1f, 1.0f}}},
        VkClearValue{.depthStencil = {0.0f, 0}}
    };

    if( MSAA )
    {
        clearValues.push_back( VkClearValue{.color = {{0.1f, 0.1f, 0.1f, 1.0f}}} );
    }

    VkRenderPassBeginInfo rpInfo
    {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        .renderPass = m_renderPass.renderPassHeadless(),
        .framebuffer = m_framebuffers[m_swapchainAcquiredIndex],
        .renderArea  {
            .offset {
                .x = 0,
                .y = 0, },
            .extent = m_extent},
        .clearValueCount = static_cast<uint32_t>( clearValues.size() ),
        .pClearValues = clearValues.data(),
    };

    vkCmdBeginRenderPass( m_commandBuffers[0], &rpInfo, VK_SUBPASS_CONTENTS_INLINE );
    fillCommandBuffer();
    vkCmdEndRenderPass( m_commandBuffers[0] );

    m_commandBuffers.end();

    std::array<VkCommandBuffer, 1> commandBuffers =
    {
        m_commandBuffers[0]
    };

    VkSubmitInfo submitInfo
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .waitSemaphoreCount = 0,
        .pWaitSemaphores = nullptr,
        .pWaitDstStageMask = 0,
        .commandBufferCount = commandBuffers.size(),
        .pCommandBuffers = commandBuffers.data(),
        .signalSemaphoreCount = 0,
        .pSignalSemaphores = nullptr,
    };

    VK_CHECK_RESULT( vkQueueSubmit( m_device.queueGraphics(), 1, &submitInfo, 0 ) );
}

/**************************************************************************************************/
void VulkanHeadless::presentNextFrame()
{
    VK_CHECK_RESULT( vkQueueWaitIdle( m_device.queueGraphics() ) );
}

