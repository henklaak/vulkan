#pragma once
#include <vulkan/vulkan.h>
#include <vector>

class Swapchain
{
public:
    Swapchain();
    virtual ~Swapchain();

    VkSwapchainKHR operator()()
    {
        return m_swapchain;
    }

    VkExtent2D extent() const
    {
        return m_extent;
    }

    std::vector<VkImageView> imageViews()
    {
        return m_imageViewsSwapchain;
    }

    void recreate(VkDevice device,
                  VkPhysicalDevice physicalDevice,
                  VkSurfaceKHR surface,
                  VkSurfaceFormatKHR surfaceFormat,
                  VkPresentModeKHR presentMode);
    void destroy();

private:
    VkDevice m_device = VK_NULL_HANDLE;
    VkSwapchainKHR m_swapchain = VK_NULL_HANDLE;
    VkExtent2D m_extent{};
    std::vector<VkImage> m_imagesSwapchain;
    std::vector<VkImageView> m_imageViewsSwapchain;
};
