#pragma once
#include <string>
#include <iostream>

#define LOG_DEBUG(...) VK::log(10, __VA_ARGS__)
#define LOG_INFO(...)  VK::log(20, __VA_ARGS__)
#define LOG_WARN(...)  VK::log(30, __VA_ARGS__)
#define LOG_ERROR(...) VK::log(40, __VA_ARGS__)

#define VK_CHECK_RESULT(f) \
{ \
    VkResult res = (f); \
    if (res != VK_SUCCESS) \
    { \
        std::cerr << "Fatal : VkResult is \"" << res << "\" in " << __FILE__ << " at line " << __LINE__ << "\n"; \
        if (res != VK_SUCCESS) \
            throw std::runtime_error("VK_CHECK_RESULT"); \
    } \
}



namespace VK
{
void log( int level, const char *txt, ... );
void reset_log();
}
