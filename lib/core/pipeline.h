#pragma once
#include <vulkan/vulkan.h>


class Utils;

class Pipeline
{
public:
    Pipeline();
    virtual ~Pipeline();

    virtual void create( Utils *utils,
                VkDevice device,
                VkSampleCountFlagBits msaa,
                VkRenderPass renderPass ) = 0;
    virtual void destroy() = 0;

    VkPipeline pipeline() const
    {
        return m_pipelineRender;
    }

    VkPipelineLayout layout() const
    {
        return m_pipelineLayoutRender;
    }

    VkDescriptorSetLayout descriptorSetlayout() const
    {
        return m_descriptorSetLayout;
    }

protected:
    Utils *m_utils = nullptr;
    VkDevice m_device = VK_NULL_HANDLE;

    VkDescriptorSetLayout m_descriptorSetLayout = VK_NULL_HANDLE;
    VkPipelineLayout m_pipelineLayoutRender = VK_NULL_HANDLE;
    VkPipeline m_pipelineRender = VK_NULL_HANDLE;

};
