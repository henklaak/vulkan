#pragma once
#include <string>
#include <vector>

#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

class WindowSdl2
{
public:
    WindowSdl2();
    virtual ~WindowSdl2();

    void create(const std::string &title);
    void destroy();
    void createSurface( VkInstance instance,
                        VkSurfaceKHR *surface );

    bool pollWindow();

    std::vector<std::string> getInstanceExtensions() const
    {
        return m_instanceExtensionNamesRequiredForSurface;
    }

private:
    SDL_Window *m_window = nullptr;
    std::vector<std::string> m_instanceExtensionNamesRequiredForSurface;
};
