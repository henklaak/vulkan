#include "sdl_swapchain.h"
#include "core_log.h"

/**************************************************************************************************/
Swapchain::Swapchain()
{}

/**************************************************************************************************/
Swapchain::~Swapchain()
{}

/**************************************************************************************************/
void Swapchain::recreate( VkDevice device,
                          VkPhysicalDevice physicalDevice,
                          VkSurfaceKHR surface,
                          VkSurfaceFormatKHR surfaceFormat,
                          VkPresentModeKHR presentMode )
{
    m_device = device;
    destroy();

    VkSurfaceCapabilitiesKHR m_surfaceCapabilities{};
    VK_CHECK_RESULT( vkGetPhysicalDeviceSurfaceCapabilitiesKHR( physicalDevice, surface, &m_surfaceCapabilities ) );
    m_extent = m_surfaceCapabilities.currentExtent;

    VkSwapchainCreateInfoKHR swapchainCI
    {
        .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .surface = surface,
        .minImageCount = m_surfaceCapabilities.minImageCount + 1,
        .imageFormat = surfaceFormat.format,
        .imageColorSpace = surfaceFormat.colorSpace,
        .imageExtent = m_extent,
        .imageArrayLayers = 1,
        .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        .imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .preTransform = m_surfaceCapabilities.currentTransform,
        .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        .presentMode = presentMode,
        .clipped = VK_TRUE,
        .oldSwapchain = VK_NULL_HANDLE
    };
    LOG_DEBUG( "vkCreateSwapchainKHR()" );
    VK_CHECK_RESULT( vkCreateSwapchainKHR( m_device, &swapchainCI, nullptr, &m_swapchain ) );

    uint32_t swapchainLength = 0;
    vkGetSwapchainImagesKHR( m_device, m_swapchain, &swapchainLength, nullptr );
    m_imagesSwapchain.resize( swapchainLength );
    vkGetSwapchainImagesKHR( m_device, m_swapchain, &swapchainLength, m_imagesSwapchain.data() );

    m_imageViewsSwapchain.resize( swapchainLength );

    for( size_t i = 0; i < m_imagesSwapchain.size(); ++i )
    {
        VkImageViewCreateInfo imageViewCI
        {
            .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            .image = m_imagesSwapchain[i],
            .viewType = VK_IMAGE_VIEW_TYPE_2D,
            .format = surfaceFormat.format,
            .subresourceRange {
                .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                .baseMipLevel = 0,
                .levelCount = 1,
                .baseArrayLayer = 0,
                .layerCount = 1}
        };

        vkCreateImageView( m_device, &imageViewCI, nullptr, &m_imageViewsSwapchain[i] );

//        m_debug.setObjectName( uint64_t( m_imageViewsSwapchain[i] ),
//                               VK_OBJECT_TYPE_IMAGE_VIEW,
//                               "ImageView SwapChain #%d",  i );
    }
}

/**************************************************************************************************/
void Swapchain::destroy()
{
    for( size_t i = 0; i < m_imageViewsSwapchain.size(); ++i )
    {
        vkDestroyImageView( m_device, m_imageViewsSwapchain[i], nullptr );
    }

    m_imageViewsSwapchain.clear();

    if( m_swapchain )
    {
        LOG_DEBUG( "vkDestroySwapchainKHR()" );
        vkDestroySwapchainKHR( m_device, m_swapchain, nullptr );
        m_swapchain = VK_NULL_HANDLE;
    }
}
