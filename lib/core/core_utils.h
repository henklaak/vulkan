#pragma once
#include <vulkan/vulkan.h>
#include <string>
#include <vector>
#include <glm/glm.hpp>
#include <tiny_gltf.h>

class Utils
{
public:
    Utils();
    virtual ~Utils();

    void create(VkInstance instance,
                VkPhysicalDevice physicalDevice,
                VkPhysicalDeviceMemoryProperties memoryProperties,
                uint32_t queueFamily,
                VkDevice device,
                VkQueue queue );
    void destroy();

    void beginLabelRegion(VkCommandBuffer cmdbuffer,
                          const char *pMarkerName,
                          glm::vec4 color );
    void endLabelRegion(VkCommandBuffer cmdBuffer );
    void setObjectName(uint64_t object,
                       VkObjectType objectType,
                       const char *txt, ... );

    VkCommandBuffer beginSingleTimeCommands();
    void endSingleTimeCommands( VkCommandBuffer commandBuffer );

    void allocateMemory( VkMemoryPropertyFlags properties,
                         VkMemoryRequirements memoryRequirements,
                         VkDeviceMemory *memory );
    void freeMemory( VkDeviceMemory *memory );

    void createBuffer( VkDeviceSize size,
                       VkBufferUsageFlags usage,
                       VkMemoryPropertyFlags properties,
                       const void *initialData,
                       VkBuffer *buffer,
                       VkDeviceMemory *memory );
    void destroyBuffer( VkBuffer *buffer,
                        VkDeviceMemory *memory );

    void mapBuffer( VkDeviceMemory memory,
                    void **mapped );
    void unmapBuffer( VkDeviceMemory memory,
                      void **mapped );

    void copyBuffer( VkBuffer src, VkBuffer dst, VkDeviceSize size );

    void flushBuffer( VkDeviceMemory memory );
    void invalidateBuffer( VkDeviceMemory memory );


    void createImage( VkFormat format,
                      VkExtent2D extent,
                      uint32_t mipLevels,
                      uint32_t arrayLayers,
                      VkSampleCountFlagBits samples,
                      VkImageTiling imageTiling,
                      VkImageUsageFlags usage,
                      VkMemoryPropertyFlags properties,
                      VkImage *image,
                      VkDeviceMemory *imageMemory );

    void destroyImage( VkImage *image,
                       VkDeviceMemory *imageMemory );

    void createImageView( VkImage image,
                          VkImageViewType viewType,
                          VkFormat format,
                          VkImageAspectFlagBits aspectFlagBits,
                          uint32_t mipLevels,
                          uint32_t arrayLayers,
                          VkImageView *imageView,
                          void *pNext = nullptr );
    void destroyImageView( VkImageView *imageView );

    void transitionImageLayout( VkCommandBuffer cmd,
                                VkImage image,
                                VkImageLayout srcLayout,
                                VkImageLayout dstLayout );
    void copyBufferToImage( VkCommandBuffer commandBuffer,
                            VkBuffer buffer,
                            VkImage image,
                            uint32_t width,
                            uint32_t height );

    VkShaderModule createShaderModule( const std::vector<char> &code );
    std::vector<char> readFile( const std::string &filename );

    void loadGltfModel( const std::string &filename,
                       tinygltf::Model *model );

public:
    VkInstance m_instance  = VK_NULL_HANDLE;
    VkPhysicalDevice m_physicalDevice = VK_NULL_HANDLE;
    VkDevice m_device = VK_NULL_HANDLE;
    uint32_t m_queueFamily = 0;
    VkPhysicalDeviceMemoryProperties m_memoryProperties;
    PFN_vkSetDebugUtilsObjectNameEXT vkSetDebugUtilsObjectNameEXT = nullptr;
    PFN_vkSetDebugUtilsObjectTagEXT vkSetDebugUtilsObjectTagEXT = nullptr;
    PFN_vkCmdBeginDebugUtilsLabelEXT vkCmdBeginDebugUtilsLabelEXT = nullptr;
    PFN_vkCmdEndDebugUtilsLabelEXT vkCmdEndDebugUtilsLabelEXT = nullptr;

    VkCommandPool m_commandPool =VK_NULL_HANDLE;
    VkQueue m_queue = VK_NULL_HANDLE;
};
