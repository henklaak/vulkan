#include "sdl_surface.h"
#include <set>
#include <vector>

#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include "core_log.h"
#include "sdl_window.h"

/**************************************************************************************************/
SdlSurface::SdlSurface()
{}

/**************************************************************************************************/
SdlSurface::~SdlSurface()
{}

/**************************************************************************************************/
void SdlSurface::create( VkInstance instance,
                         VkPhysicalDevice physicalDevice,
                         WindowSdl2 *window,
                         bool VSYNC )
{
    m_instance = instance;
    m_physicalDevice = physicalDevice;

    LOG_DEBUG( "createSurface()" );
    window->createSurface( m_instance, &m_surface );

    VkBool32 presentSupport = VK_FALSE;
    vkGetPhysicalDeviceSurfaceSupportKHR( m_physicalDevice, 0, m_surface, &presentSupport );

    uint32_t formatCount = 0;
    vkGetPhysicalDeviceSurfaceFormatsKHR( m_physicalDevice, m_surface, &formatCount, nullptr );
    std::vector<VkSurfaceFormatKHR> surfaceFormats( formatCount );
    vkGetPhysicalDeviceSurfaceFormatsKHR( m_physicalDevice, m_surface, &formatCount, surfaceFormats.data() );

    if( formatCount == 0 )
    {
        throw std::runtime_error( "vkGetPhysicalDeviceSurfaceFormatsKHR()" );
    }

    // Pick SRGB format
    m_surfaceFormat = surfaceFormats[0];

    uint32_t presentModeCount = 0;
    vkGetPhysicalDeviceSurfacePresentModesKHR( m_physicalDevice, m_surface, &presentModeCount, nullptr );
    std::vector<VkPresentModeKHR> presentModes( presentModeCount );
    vkGetPhysicalDeviceSurfacePresentModesKHR( m_physicalDevice, m_surface, &presentModeCount, presentModes.data() );

    std::vector<VkPresentModeKHR> candidates =
    {
        VK_PRESENT_MODE_IMMEDIATE_KHR,
        VK_PRESENT_MODE_FIFO_KHR,
    };

    if( VSYNC )
    {
        candidates.insert( candidates.begin(), VK_PRESENT_MODE_FIFO_KHR );
    }

    for( auto &candidate : candidates )
    {
        if( presentModes.end() != find( presentModes.begin(), presentModes.end(), candidate ) )
        {
            m_presentModeSurface = candidate;
            break;
        }
    }
}

/**************************************************************************************************/
void SdlSurface::destroy()
{
    LOG_DEBUG( "vkDestroySurfaceKHR()" );
    vkDestroySurfaceKHR( m_instance, m_surface, nullptr );
    m_surface = VK_NULL_HANDLE;
}

/**************************************************************************************************/
VkSurfaceFormatKHR SdlSurface::surfaceFormat()
{
    return m_surfaceFormat;
}

/************************************VkExtent2D**************************************************************/
VkExtent2D SdlSurface::extent()
{
    VK_CHECK_RESULT( vkGetPhysicalDeviceSurfaceCapabilitiesKHR( m_physicalDevice, m_surface, &m_surfaceCapabilities ) );
    return m_surfaceCapabilities.currentExtent;
}

/**************************************************************************************************/
uint32_t SdlSurface::minImageCount()
{
    VK_CHECK_RESULT( vkGetPhysicalDeviceSurfaceCapabilitiesKHR( m_physicalDevice, m_surface, &m_surfaceCapabilities ) );
    return m_surfaceCapabilities.minImageCount;
}

/**************************************************************************************************/
VkSurfaceTransformFlagBitsKHR SdlSurface::currentTransform()
{
    VK_CHECK_RESULT( vkGetPhysicalDeviceSurfaceCapabilitiesKHR( m_physicalDevice, m_surface, &m_surfaceCapabilities ) );
    return m_surfaceCapabilities.currentTransform;
}

/**************************************************************************************************/
VkPresentModeKHR SdlSurface::presentMode() const
{
    return m_presentModeSurface;
}
