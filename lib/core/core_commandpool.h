#pragma once
#include <vulkan/vulkan.h>

class CommandPool
{
public:
    CommandPool();
    virtual ~CommandPool();

    VkCommandPool operator()()
    {
        return m_commandPoolGraphics;
    }

    void create(VkPhysicalDevice physicalDevice,
                VkDevice device,
                uint32_t queueFamilyGraphics );
    void destroy();

private:
    VkDevice m_device = VK_NULL_HANDLE;

    VkCommandPool m_commandPoolGraphics=VK_NULL_HANDLE;
};
