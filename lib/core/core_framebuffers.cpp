#include "core_framebuffers.h"
#include "core_log.h"
#include "core_utils.h"

/**************************************************************************************************/
Framebuffers::Framebuffers()
{}

/**************************************************************************************************/
Framebuffers::~Framebuffers()
{}

/**************************************************************************************************/
void Framebuffers::recreateColorAttachment()
{
    destroyColorAttachment();
    m_utils->createImage( m_format,
                          m_extent,
                          1,
                          m_nrViews,
                          m_msaa,
                          VK_IMAGE_TILING_OPTIMAL,
                          VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                          &m_imageColor,
                          &m_memoryColor );
    m_utils->setObjectName( uint64_t( m_imageColor ), VK_OBJECT_TYPE_IMAGE, "Image ColorAttachment" );

    m_utils->createImageView( m_imageColor,
                              VK_IMAGE_VIEW_TYPE_2D,
                              m_format,
                              VK_IMAGE_ASPECT_COLOR_BIT,
                              1,
                              m_nrViews,
                              &m_imageViewColor );
    m_utils->setObjectName( uint64_t( m_imageViewColor ), VK_OBJECT_TYPE_IMAGE_VIEW, "ImageView ColorAttachment" );
}

/**************************************************************************************************/
void Framebuffers::destroyColorAttachment()
{
    m_utils->destroyImageView( &m_imageViewColor );
    m_utils->destroyImage( &m_imageColor, &m_memoryColor );
}

/**************************************************************************************************/
void Framebuffers::recreateDepthAttachment()
{
    destroyDepthAttachment();
    m_utils->createImage( VK_FORMAT_D32_SFLOAT,
                          m_extent,
                          1,
                          m_nrViews,
                          m_msaa,
                          VK_IMAGE_TILING_OPTIMAL,
                          VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
                          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                          &m_imageDepth,
                          &m_memoryDepth );
    m_utils->setObjectName( uint64_t( m_imageDepth ), VK_OBJECT_TYPE_IMAGE, "Image DepthAttachment" );

    m_utils->createImageView( m_imageDepth,
                              VK_IMAGE_VIEW_TYPE_2D,
                              VK_FORMAT_D32_SFLOAT,
                              VK_IMAGE_ASPECT_DEPTH_BIT,
                              1,
                              m_nrViews,
                              &m_imageViewDepth );
    m_utils->setObjectName( uint64_t( m_imageViewDepth ), VK_OBJECT_TYPE_IMAGE_VIEW, "ImageView DepthAttachment" );
}

/**************************************************************************************************/
void Framebuffers::destroyDepthAttachment()
{
    m_utils->destroyImageView( &m_imageViewDepth );
    m_utils->destroyImage( &m_imageDepth, &m_memoryDepth );
}


/**************************************************************************************************/
void Framebuffers::recreate( Utils *utils,
                             VkDevice device,
                             VkRenderPass renderPass,
                             std::vector<VkImageView> imageViews,
                             VkFormat format,
                             VkExtent2D extent,
                             VkSampleCountFlagBits msaa )
{
    m_utils = utils;
    m_device = device;
    m_format = format;
    m_extent = extent;
    m_msaa = msaa;
    bool MSAA = ( m_msaa != VK_SAMPLE_COUNT_1_BIT );

    destroy();

    recreateColorAttachment();
    recreateDepthAttachment();

    m_frameBuffers.resize( imageViews.size() );

    for( size_t i = 0; i < imageViews.size(); ++i )
    {
        std::vector<VkImageView> attachments;
        attachments.push_back( imageViews[i] );
        attachments.push_back( m_imageViewDepth );

        if( MSAA )
        {
            attachments.push_back( m_imageViewColor );
        }

        VkFramebufferCreateInfo fb_info =
        {
            .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
            .renderPass = renderPass,
            .attachmentCount = static_cast<uint32_t>( attachments.size() ),
            .pAttachments = attachments.data(),
            .width = extent.width,
            .height = extent.height,
            .layers = 1
        };
        LOG_DEBUG( "vkCreateFramebuffer()" );
        VK_CHECK_RESULT( vkCreateFramebuffer( m_device, &fb_info, nullptr, &m_frameBuffers[i] ) );
    }
}

/**************************************************************************************************/
void Framebuffers::destroy()
{
    for( size_t i = 0; i < m_frameBuffers.size(); ++i )
    {
        LOG_DEBUG( "vkDestroyFramebuffer()" );
        vkDestroyFramebuffer( m_device, m_frameBuffers[i], nullptr );
    }

    destroyDepthAttachment();
    destroyColorAttachment();
}



