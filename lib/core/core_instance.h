#pragma once
#include <vulkan/vulkan.h>
#include <string>
#include <vector>

class Instance
{
public:
    Instance();
    virtual ~Instance();

    VkInstance operator()()
    {
        return m_instance;
    }

    void create( std::vector<std::string> extensions, bool debug );
    void destroy();

private:
    void initRequestedInstanceLayers( bool debug );
    void initRequestedInstanceExtensions();

    std::vector<std::string> m_instanceLayerNamesRequested;
    std::vector<std::string> m_instanceExtensionNamesRequested;
    std::vector<std::string> m_instanceExtensionNamesAdditional;
    VkInstance m_instance;
};
