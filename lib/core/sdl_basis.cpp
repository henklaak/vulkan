#include "sdl_basis.h"

#include <cstring>
#include <fstream>
#include <sstream>
#include <stdarg.h>
#include <sys/time.h>
#include "core_log.h"

/**************************************************************************************************/
VulkanSdl::VulkanSdl( const std::string &windowTitle )
    : m_windowTitle( windowTitle )
{
}

/**************************************************************************************************/
VulkanSdl::~VulkanSdl()
{
}

/**************************************************************************************************/
void VulkanSdl::create()
{
    VK::reset_log();
    m_window.create( m_windowTitle );
    m_instance.create( m_window.getInstanceExtensions(), true );
    m_messenger.create( m_instance() );
    m_physicalDevice.select( m_instance(), MSAA );
    m_surface.create( m_instance(), m_physicalDevice(), &m_window, VSYNC );
    m_device.create( m_physicalDevice(), m_physicalDevice.queueFamilyGraphics(), SWAPCHAIN );
    m_utils.create( m_instance(), m_physicalDevice(), m_physicalDevice.memoryProperties(),
                    m_physicalDevice.queueFamilyGraphics(), m_device(), m_device.queueGraphics() );
    m_commandPool.create( m_physicalDevice(), m_device(), m_physicalDevice.queueFamilyGraphics() );
    m_commandBuffers.create( m_device(), m_commandPool(), MAX_FRAMES_IN_FLIGHT );
    m_renderPass.create( m_device(), m_surface.surfaceFormat().format, m_physicalDevice.msaa() );
    m_swapchain.recreate( m_device(), m_physicalDevice(), m_surface(), m_surface.surfaceFormat(),
                          m_surface.presentMode() );
    m_framebuffers.recreate( &m_utils, m_device(), m_renderPass.renderPassSwapchain(),
                             m_swapchain.imageViews(), m_surface.surfaceFormat().format,
                             m_swapchain.extent(), m_physicalDevice.msaa() );
    m_sync.create( m_device(), MAX_FRAMES_IN_FLIGHT );

    prepare();
}

/**************************************************************************************************/
void VulkanSdl::destroy()
{
    vkDeviceWaitIdle( m_device() );

    unprepare();

    m_sync.destroy();
    m_framebuffers.destroy();
    m_swapchain.destroy();
    m_renderPass.destroy();
    m_commandBuffers.destroy();
    m_commandPool.destroy();
    m_utils.destroy();
    m_device.destroy();
    m_surface.destroy();
    m_messenger.destroy();
    m_instance.destroy();
    m_window.destroy();
}


/**************************************************************************************************/
bool VulkanSdl::acquireNextFrame( )
{
    VkFence fence = m_sync.fenceInFlight( m_currentFrame );
    VK_CHECK_RESULT( vkWaitForFences( m_device(), 1, &fence, VK_TRUE, UINT64_MAX ) );

    VkResult result = vkAcquireNextImageKHR( m_device(),
                      m_swapchain(),
                      UINT64_MAX,
                      m_sync.semaphoreImageAvailable( m_currentFrame ),
                      VK_NULL_HANDLE,
                      &m_swapchainAcquiredIndex );

    if( result == VK_ERROR_OUT_OF_DATE_KHR ||
            result == VK_SUBOPTIMAL_KHR )
    {
        vkDeviceWaitIdle( m_device() );
        m_swapchain.recreate( m_device(),
                              m_physicalDevice(),
                              m_surface(),
                              m_surface.surfaceFormat(),
                              m_surface.presentMode() );
        m_framebuffers.recreate( &m_utils,
                                 m_device(),
                                 m_renderPass.renderPassSwapchain(),
                                 m_swapchain.imageViews(),
                                 m_surface.surfaceFormat().format,
                                 m_swapchain.extent(),
                                 m_physicalDevice.msaa() );
        return false;
    }
    else
    {
        VK_CHECK_RESULT( result );
    }

    return true;
}

/**************************************************************************************************/
void VulkanSdl::renderNextFrame( )
{
    VkFence fence = m_sync.fenceInFlight( m_currentFrame );
    VK_CHECK_RESULT( vkResetFences( m_device(), 1, &fence ) );

    m_commandBuffers.begin( m_currentFrame, m_swapchain.extent() );

    fillCommandBufferPreRenderPass();

    std::vector<VkClearValue> clearValues
    {
        VkClearValue{.color = {{0.1f, 0.1f, 0.1f, 1.0f}}},
        VkClearValue{.depthStencil = {0.0f, 0}}
    };

    if( MSAA )
    {
        clearValues.push_back( VkClearValue{.color = {{0.1f, 0.1f, 0.1f, 1.0f}}} );
    }

    VkRenderPassBeginInfo rpInfo
    {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        .renderPass = m_renderPass.renderPassSwapchain(),
        .framebuffer = m_framebuffers[m_swapchainAcquiredIndex],
        .renderArea  {
            .offset {
                .x = 0,
                .y = 0, },
            .extent = m_swapchain.extent() },
        .clearValueCount = static_cast<uint32_t>( clearValues.size() ),
        .pClearValues = clearValues.data(),
    };

    vkCmdBeginRenderPass( m_commandBuffers[m_currentFrame], &rpInfo, VK_SUBPASS_CONTENTS_INLINE );
    fillCommandBuffer();
    vkCmdEndRenderPass( m_commandBuffers[m_currentFrame] );

    m_commandBuffers.end();

    std::array<VkSemaphore, 1> waitSemaphores =
    {
        m_sync.semaphoreImageAvailable( m_currentFrame )
    };
    VkPipelineStageFlags waitStageMask[] =
    {
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
    };
    std::array<VkCommandBuffer, 1> commandBuffers =
    {
        m_commandBuffers[m_currentFrame]
    };
    std::array<VkSemaphore, 1> renderedSemaphores =
    {
        m_sync.semaphoreRenderFinished( m_currentFrame )
    };

    VkSubmitInfo submitInfo
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .waitSemaphoreCount = waitSemaphores.size(),
        .pWaitSemaphores = waitSemaphores.data(),
        .pWaitDstStageMask = waitStageMask,
        .commandBufferCount = commandBuffers.size(),
        .pCommandBuffers = commandBuffers.data(),
        .signalSemaphoreCount = renderedSemaphores.size(),
        .pSignalSemaphores = renderedSemaphores.data(),
    };

    vkQueueSubmit( m_device.queueGraphics(), 1, &submitInfo, m_sync.fenceInFlight( m_currentFrame ) );
}

/**************************************************************************************************/
void VulkanSdl::presentNextFrame()
{
    std::array<VkSemaphore, 1> waitSemaphores =
    {
        m_sync.semaphoreRenderFinished( m_currentFrame )
    };

    std::array<VkSwapchainKHR, 1> swapChains =
    {
        m_swapchain()
    };
#ifdef USE_SDL2
#endif


    VkPresentInfoKHR presentInfo
    {
        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .waitSemaphoreCount = waitSemaphores.size(),
        .pWaitSemaphores = waitSemaphores.data(),
        .swapchainCount = swapChains.size(),
        .pSwapchains = swapChains.data(),
        .pImageIndices = &m_swapchainAcquiredIndex
    };

    VkResult result = vkQueuePresentKHR( m_device.queueGraphics(), &presentInfo );

    if( result == VK_ERROR_OUT_OF_DATE_KHR or
            result == VK_SUBOPTIMAL_KHR )
    {
        vkDeviceWaitIdle( m_device() );
        m_swapchain.recreate( m_device(),
                              m_physicalDevice(),
                              m_surface(),
                              m_surface.surfaceFormat(),
                              m_surface.presentMode() );
        m_framebuffers.recreate( &m_utils,
                                 m_device(),
                                 m_renderPass.renderPassSwapchain(),
                                 m_swapchain.imageViews(),
                                 m_surface.surfaceFormat().format,
                                 m_swapchain.extent(),
                                 m_physicalDevice.msaa() );
    }
    else
    {
        VK_CHECK_RESULT( result );
        m_currentFrame = ( m_currentFrame + 1 ) % MAX_FRAMES_IN_FLIGHT;
    }
}

