#include "core_renderpasses.h"
#include <array>
#include <vector>
#include "core_log.h"

/**************************************************************************************************/
RenderPass::RenderPass()
{}

/**************************************************************************************************/
RenderPass::~RenderPass()
{}

/**************************************************************************************************/
void RenderPass::create( VkDevice device,
                         VkFormat format,
                         VkSampleCountFlagBits msaa )
{
    m_device = device;
    bool MSAA = ( msaa != VK_SAMPLE_COUNT_1_BIT );

    VkAttachmentDescription output_attachment
    {
        .format = format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = MSAA ? VK_ATTACHMENT_LOAD_OP_DONT_CARE : VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
    };

    VkAttachmentDescription depth_attachment
    {
        .format = VK_FORMAT_D32_SFLOAT,
        .samples = msaa,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    };

    VkAttachmentDescription color_attachment
    {
        .format = format,
        .samples = msaa,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    };

    VkAttachmentReference output_attachment_ref
    {
        .attachment = 0,
        .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    };

    VkAttachmentReference depth_attachment_ref
    {
        .attachment = 1,
        .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    };

    VkAttachmentReference color_attachment_ref
    {
        .attachment = 2,
        .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    };

    std::vector<VkAttachmentDescription> attachments =
    {
        output_attachment,
        depth_attachment
    };

    if( MSAA )
    {
        attachments.push_back( color_attachment );
    }

    std::array<VkSubpassDescription, 1> subpasses =
    {
        VkSubpassDescription {
            .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
            .colorAttachmentCount = 1,
            .pColorAttachments = MSAA ? &color_attachment_ref :&output_attachment_ref,
            .pResolveAttachments = MSAA ? &output_attachment_ref : nullptr,
            .pDepthStencilAttachment = &depth_attachment_ref,
        }
    };

    std::array<VkSubpassDependency, 1> dependencies
    {
        VkSubpassDependency{
            .srcSubpass = VK_SUBPASS_EXTERNAL,
            .dstSubpass = 0,
            .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            .srcAccessMask = 0,
            .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT
        }
    };

    VkRenderPassCreateInfo renderPassCI
    {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .attachmentCount = static_cast<uint32_t>( attachments.size() ),
        .pAttachments = attachments.data(),
        .subpassCount = subpasses.size(),
        .pSubpasses = subpasses.data(),
        .dependencyCount = dependencies.size(),
        .pDependencies = dependencies.data()
    };

    LOG_DEBUG( "vkCreateRenderPass()" );
    VK_CHECK_RESULT( vkCreateRenderPass( m_device, &renderPassCI, nullptr, &m_renderPassSwapchain ) );

    attachments[0].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    VK_CHECK_RESULT( vkCreateRenderPass( m_device, &renderPassCI, nullptr, &m_renderPassHeadless ) );
}

/**************************************************************************************************/
void RenderPass::destroy()
{
    LOG_DEBUG( "vkDestroyRenderPass()" );
    vkDestroyRenderPass( m_device, m_renderPassHeadless, nullptr );
    m_renderPassHeadless = VK_NULL_HANDLE;
    vkDestroyRenderPass( m_device, m_renderPassSwapchain, nullptr );
    m_renderPassSwapchain = VK_NULL_HANDLE;
}
