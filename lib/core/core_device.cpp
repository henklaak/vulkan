#include "core_device.h"
#include <array>
#include <vector>
#include "core_log.h"

/**************************************************************************************************/
Device::Device()
{}

/**************************************************************************************************/
Device::~Device()
{}

/**************************************************************************************************/
void Device::initRequestedDeviceLayers()
{
    LOG_DEBUG( "vkEnumerateDeviceLayerProperties():" );

    uint32_t count = 0;
    VK_CHECK_RESULT( vkEnumerateDeviceLayerProperties( m_physicalDevice, &count, nullptr ) );
    std::vector<VkLayerProperties> layers( count );
    VK_CHECK_RESULT( vkEnumerateDeviceLayerProperties( m_physicalDevice, &count, layers.data() ) );

    for( auto &layer : layers )
    {
        LOG_DEBUG( "  %s", layer.layerName );
    }

    LOG_DEBUG( "requestedDeviceLayerNames:" );

    for( auto &requestedDeviceLayerName : m_deviceLayerNamesRequested )
    {
        LOG_DEBUG( "  %s", requestedDeviceLayerName.c_str() );
    }
}

/**************************************************************************************************/
void Device::initRequestedDeviceExtensions( bool swapchain )
{
    LOG_DEBUG( "vkEnumerateDeviceExtensionProperties():" );

    uint32_t count = 0;
    VK_CHECK_RESULT( vkEnumerateDeviceExtensionProperties( m_physicalDevice, nullptr, &count, nullptr ) );
    std::vector<VkExtensionProperties> extensions( count );
    VK_CHECK_RESULT( vkEnumerateDeviceExtensionProperties( m_physicalDevice, nullptr, &count, extensions.data() ) );

    for( auto &extension : extensions )
    {
        LOG_DEBUG( "  %s", extension.extensionName );
    }

    if( swapchain )
    {
        m_deviceExtensionNamesRequested.push_back( VK_KHR_SWAPCHAIN_EXTENSION_NAME );
    }

    LOG_DEBUG( "deviceExtensionNamesRequested:" );

    for( auto &requestedDeviceExtensionName : m_deviceExtensionNamesRequested )
    {
        LOG_DEBUG( "  %s", requestedDeviceExtensionName.c_str() );
    }
}

/**************************************************************************************************/
void Device::initRequestedDeviceFeatures()
{
    m_features.fillModeNonSolid = VK_TRUE;

    static VkPhysicalDeviceMultiviewFeaturesKHR physicalDeviceMultiviewFeatures
    {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES_KHR,
        .multiview = VK_TRUE
    };

    static VkPhysicalDeviceSamplerYcbcrConversionFeatures physicalDeviceSamplerYcbcrConversionFeatures
    {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_YCBCR_CONVERSION_FEATURES,
        .pNext = &physicalDeviceMultiviewFeatures,
        .samplerYcbcrConversion = VK_TRUE,
    };

    m_deviceCreateInfoChain = &physicalDeviceSamplerYcbcrConversionFeatures;
}

/**************************************************************************************************/
void Device::create( VkPhysicalDevice physicalDevice,
                     uint32_t queueFamilyGraphics,
                     bool swapchain )
{
    m_physicalDevice = physicalDevice;

    initRequestedDeviceLayers();
    initRequestedDeviceExtensions( swapchain );
    initRequestedDeviceFeatures();

    std::vector<const char *> edln;

    for( size_t i = 0; i < m_deviceLayerNamesRequested.size(); ++i )
    {
        edln.push_back( m_deviceLayerNamesRequested[i].c_str() );
    }

    std::vector<const char *> eden;

    for( size_t i = 0; i < m_deviceExtensionNamesRequested.size(); ++i )
    {
        eden.push_back( m_deviceExtensionNamesRequested[i].c_str() );
    }

    std::array<float, 1> queuePriority = {1.0f};
    std::array<VkDeviceQueueCreateInfo, 1> queueCreateInfos
    {
        VkDeviceQueueCreateInfo {
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .queueFamilyIndex = queueFamilyGraphics,
            .queueCount = queuePriority.size(),
            .pQueuePriorities = queuePriority.data()
        }
    };

    VkDeviceCreateInfo createInfo
    {
        .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .pNext = m_deviceCreateInfoChain,
        .queueCreateInfoCount = queueCreateInfos.size(),
        .pQueueCreateInfos = queueCreateInfos.data(),
        .enabledLayerCount = static_cast<uint32_t>( edln.size() ),
        .ppEnabledLayerNames = edln.data(),
        .enabledExtensionCount = static_cast<uint32_t>( eden.size() ),
        .ppEnabledExtensionNames = eden.data(),
        .pEnabledFeatures = &m_features,
    };

    VK_CHECK_RESULT( vkCreateDevice( m_physicalDevice, &createInfo, nullptr, &m_device ) );
    vkGetDeviceQueue( m_device, queueFamilyGraphics, 0, &m_queueGraphics );
}

/**************************************************************************************************/
void Device::destroy()
{
    if( m_device )
    {
        LOG_DEBUG( "vkDestroyDevice()" );
        vkDestroyDevice( m_device, nullptr );
        m_device = VK_NULL_HANDLE;
    }
}

/**************************************************************************************************/
VkQueue Device::queueGraphics() const
{
    return m_queueGraphics;
}
