#pragma once
#include <vulkan/vulkan.h>

class RenderPass
{
public:
    RenderPass();
    virtual ~RenderPass();

    VkRenderPass renderPassSwapchain()
    {
        return m_renderPassSwapchain;
    }

    VkRenderPass renderPassHeadless()
    {
        return m_renderPassHeadless;
    }

    void create(VkDevice device,
                VkFormat format,
                VkSampleCountFlagBits msaa );
    void destroy();

private:
    VkDevice m_device = VK_NULL_HANDLE;

    VkRenderPass m_renderPassSwapchain = VK_NULL_HANDLE;
    VkRenderPass m_renderPassHeadless = VK_NULL_HANDLE;

};
