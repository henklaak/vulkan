#pragma once
#include <array>
#include <iostream>
#include <stdexcept>
#include <vector>

#include <vulkan/vulkan.h>
#include <glm/glm.hpp>

#include <tiny_gltf.h>

#include "sdl_window.h"
#include "core_instance.h"
#include "core_messenger.h"
#include "core_physical_device.h"
#include "sdl_surface.h"
#include "core_device.h"
#include "core_utils.h"
#include "core_commandpool.h"
#include "sdl_commandbuffers.h"
#include "core_renderpasses.h"
#include "sdl_swapchain.h"
#include "core_framebuffers.h"
#include "sdl_sync.h"

class VulkanHeadless
{
public:

    bool MSAA  = true;
    bool FXAA  = false;
    bool DEBUG = true;

    VulkanHeadless();
    virtual ~VulkanHeadless();

    virtual void create() ;
    virtual void destroy();

    virtual void prepare() = 0;
    virtual void fillCommandBufferPreRenderPass() = 0;
    virtual void fillCommandBuffer() = 0;
    virtual void unprepare() = 0;

    Instance m_instance;
    Messenger m_messenger;
    PhysicalDevice m_physicalDevice;
    Device m_device;
    Utils m_utils;
    CommandPool m_commandPool;
    CommandBuffers m_commandBuffers;
    RenderPass m_renderPass;
    Framebuffers m_framebuffers;
    Sync m_sync;

    // vulkan_render
    bool acquireNextFrame();
    void renderNextFrame();
    void presentNextFrame();

    uint32_t m_swapchainAcquiredIndex = 0;

    VkFormat m_format;
    VkExtent2D m_extent;
    VkImage m_imageColor;
    VkDeviceMemory m_memoryColor;
    VkImageView m_imageViewColor;

};

