#include "core_instance.h"
#include "core_log.h"

/**************************************************************************************************/
Instance::Instance()
{}

/**************************************************************************************************/
Instance::~Instance()
{}

/**************************************************************************************************/
void Instance::initRequestedInstanceLayers( bool debug )
{
    uint32_t count = 0;
    VK_CHECK_RESULT( vkEnumerateInstanceLayerProperties( &count, nullptr ) );
    std::vector<VkLayerProperties> availableLayers( count );
    VK_CHECK_RESULT( vkEnumerateInstanceLayerProperties( &count, availableLayers.data() ) );

    m_instanceLayerNamesRequested.clear();

    if( debug )
    {
        m_instanceLayerNamesRequested.push_back( "VK_LAYER_KHRONOS_validation" );
    }

    LOG_DEBUG( "instanceLayers available:" );

    for( auto &availableLayer : availableLayers )
    {
        LOG_DEBUG( "  %s", availableLayer.layerName );
    }

    LOG_DEBUG( "instanceLayers requested:" );

    for( auto &requestedInstanceLayerName : m_instanceLayerNamesRequested )
    {
        LOG_DEBUG( "  %s", requestedInstanceLayerName.c_str() );
    }
}

/**************************************************************************************************/
void Instance::initRequestedInstanceExtensions()
{
    uint32_t count = 0;
    VK_CHECK_RESULT( vkEnumerateInstanceExtensionProperties( nullptr, &count, nullptr ) );
    std::vector<VkExtensionProperties> availableExtensions( count );
    VK_CHECK_RESULT( vkEnumerateInstanceExtensionProperties( nullptr, &count, availableExtensions.data() ) );

    m_instanceExtensionNamesRequested.clear();

    for( auto &layer : m_instanceExtensionNamesAdditional )
    {
        m_instanceExtensionNamesRequested.push_back( layer );
    }

    m_instanceExtensionNamesRequested.push_back( VK_EXT_DEBUG_UTILS_EXTENSION_NAME );
    m_instanceExtensionNamesRequested.push_back( VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME );

    LOG_DEBUG( "instanceExtensions available:" );

    for( auto &availableExtension : availableExtensions )
    {
        LOG_DEBUG( "  %s", availableExtension.extensionName );
    }

    LOG_DEBUG( "instanceExtensions requested:" );

    for( auto &requestedInstanceExtensionName : m_instanceExtensionNamesRequested )
    {
        LOG_DEBUG( "  %s", requestedInstanceExtensionName.c_str() );
    }
}


/**************************************************************************************************/
void Instance::create( std::vector<std::string> extensions, bool debug )
{
    m_instanceExtensionNamesAdditional = extensions;
    initRequestedInstanceLayers( debug );
    initRequestedInstanceExtensions();

    uint32_t apiVersion = 0;
    vkEnumerateInstanceVersion( &apiVersion );
    LOG_DEBUG( "API version  %d.%d.%d (variant %d)",
               VK_API_VERSION_MAJOR( apiVersion ),
               VK_API_VERSION_MINOR( apiVersion ),
               VK_API_VERSION_PATCH( apiVersion ),
               VK_API_VERSION_VARIANT( apiVersion )
             );

    VkApplicationInfo appInfo
    {
        .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pApplicationName = "Vulkan App",
        .applicationVersion = VK_MAKE_API_VERSION( 0, 1, 0, 0 ),
        .pEngineName = "No Engine",
        .engineVersion = VK_MAKE_API_VERSION( 0, 1, 0, 0 ),
        .apiVersion = VK_MAKE_API_VERSION( 0, 1, 3, 204 )
    };

    std::vector<const char *> eiln;

    for( size_t i = 0; i < m_instanceLayerNamesRequested.size(); ++i )
    {
        eiln.push_back( m_instanceLayerNamesRequested[i].c_str() );
    }

    std::vector<const char *> eien;

    for( size_t i = 0; i < m_instanceExtensionNamesRequested.size(); ++i )
    {
        eien.push_back( m_instanceExtensionNamesRequested[i].c_str() );
    }

    VkInstanceCreateInfo createInfo
    {
        .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pApplicationInfo = &appInfo,
        .enabledLayerCount = static_cast<uint32_t>( eiln.size() ),
        .ppEnabledLayerNames = eiln.data(),
        .enabledExtensionCount = static_cast<uint32_t>( eien.size() ),
        .ppEnabledExtensionNames = eien.data()
    };

    VK_CHECK_RESULT( vkCreateInstance( &createInfo, nullptr, &m_instance ) );
}

/**************************************************************************************************/
void Instance::destroy()
{
    vkDestroyInstance( m_instance, nullptr );
}

