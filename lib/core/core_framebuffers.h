#pragma once
#include <vulkan/vulkan.h>
#include <vector>

class Utils;

class Framebuffers
{
public:
    Framebuffers();
    virtual ~Framebuffers();

    VkFramebuffer operator[](int i)
    {
        return m_frameBuffers[i];
    }

    void recreate(Utils *utils,
                  VkDevice device,
                  VkRenderPass renderPass,
                  std::vector<VkImageView> imageViews,
                  VkFormat format,
                  VkExtent2D extent,
                  VkSampleCountFlagBits msaa );
    void destroy();

private:
    // vulkan_framebuffer
    void recreateColorAttachment();
    void destroyColorAttachment();
    void recreateDepthAttachment();
    void destroyDepthAttachment();

    VkDevice m_device = VK_NULL_HANDLE;

    VkFormat m_format;
    VkExtent2D m_extent;
    VkSampleCountFlagBits m_msaa;


    VkImage m_imageColor = VK_NULL_HANDLE;
    VkDeviceMemory m_memoryColor = VK_NULL_HANDLE;
    VkImageView m_imageViewColor = VK_NULL_HANDLE;
    VkImage m_imageDepth = VK_NULL_HANDLE;
    VkDeviceMemory m_memoryDepth = VK_NULL_HANDLE;
    VkImageView m_imageViewDepth = VK_NULL_HANDLE;
    std::vector<VkFramebuffer> m_frameBuffers;
    uint32_t m_nrViews = 1;

    Utils *m_utils = nullptr;
};
