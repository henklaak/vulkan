#include "core_physical_device.h"
#include <bit>
#include <cmath>
#include <vector>
#include "core_log.h"


/**************************************************************************************************/
PhysicalDevice::PhysicalDevice()
{}

/**************************************************************************************************/
PhysicalDevice::~PhysicalDevice()
{}

/**************************************************************************************************/
void PhysicalDevice::getMemoryProperties()
{
    vkGetPhysicalDeviceMemoryProperties( m_physicalDevice, &m_memoryProperties );
    LOG_DEBUG( "memoryHeaps:" );

    for( size_t i = 0; i < m_memoryProperties.memoryHeapCount; ++i )
    {
        LOG_DEBUG( "  heap: %d", i );
        LOG_DEBUG( "   flags: 0x%08x", m_memoryProperties.memoryHeaps[i].flags );
        LOG_DEBUG( "   size: 0x%016llx (%lld MB)", m_memoryProperties.memoryHeaps[i].size,
                   m_memoryProperties.memoryHeaps[i].size / ( 1024 * 1024 ) );
    }

    LOG_DEBUG( "memoryTypes:" );

    for( size_t i = 0; i < m_memoryProperties.memoryTypeCount; ++i )
    {
        LOG_DEBUG( "  type: %d", i );
        LOG_DEBUG( "   flags: 0x%08x", m_memoryProperties.memoryTypes[i].propertyFlags );
        LOG_DEBUG( "   heap: %d", m_memoryProperties.memoryTypes[i].heapIndex );
    }
}

void PhysicalDevice::getQueueFamilies()
{
    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties( m_physicalDevice, &queueFamilyCount, nullptr );
    std::vector<VkQueueFamilyProperties> queueFamilies( queueFamilyCount );
    vkGetPhysicalDeviceQueueFamilyProperties( m_physicalDevice, &queueFamilyCount, queueFamilies.data() );

    // Find first one that supports VK_QUEUE_GRAPHICS_BIT
    for( int i = 0; i < queueFamilies.size(); ++i )
    {
        if( queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT )
        {
            m_queueFamilyGraphics = i;
            break;
        }
    }

    // fall back to using same queue
    if( m_queueFamilyTransfer == UINT32_MAX )
    {
        m_queueFamilyTransfer = m_queueFamilyGraphics;
    }

    // fall back to using same queue
    if( m_queueFamilyCompute == UINT32_MAX )
    {
        m_queueFamilyCompute = m_queueFamilyGraphics;
    }

    for( auto &queueFamily : queueFamilies )
    {
        LOG_DEBUG( "  flags: 0x%08x", queueFamily.queueFlags );
        LOG_DEBUG( "  count: %d", queueFamily.queueCount );
    }
}

void PhysicalDevice::getFormats()
{
    std::vector<VkFormat> formats;

    for( int i = VK_OBJECT_TYPE_UNKNOWN; i <= VK_FORMAT_D32_SFLOAT_S8_UINT; ++i )
    {
        formats.push_back( VkFormat( i ) );
    }

    for( auto &format : formats )
    {
        VkFormatProperties formatProperties{};
        vkGetPhysicalDeviceFormatProperties( m_physicalDevice, format, &formatProperties );

        VkFormatFeatureFlagBits test = VK_FORMAT_FEATURE_STORAGE_IMAGE_BIT;
        auto a1 = formatProperties.linearTilingFeatures & test;
        auto a2 = formatProperties.optimalTilingFeatures & test;
        auto a3 = formatProperties.bufferFeatures & test;

        if( a1 || a2 || a3 )
        {
            //LOG_DEBUG( "%08lx Has 0x%08lx", format, test );
        }
    }
}

void PhysicalDevice::select( VkInstance instance, bool MSAA )
{
    m_instance = instance;

    LOG_DEBUG( "vkEnumeratePhysicalDevices():" );

    std::vector<VkPhysicalDevice> availablePhysicalDevices;
    uint32_t deviceCount = 0;
    VK_CHECK_RESULT( vkEnumeratePhysicalDevices( m_instance, &deviceCount, nullptr ) );
    availablePhysicalDevices.resize( deviceCount, VK_NULL_HANDLE );
    VK_CHECK_RESULT( vkEnumeratePhysicalDevices( m_instance, &deviceCount, availablePhysicalDevices.data() ) );

    m_physicalDevice = availablePhysicalDevices[0];

    for( auto &availablePhysicalDevice : availablePhysicalDevices )
    {
        VkPhysicalDeviceProperties properties;
        vkGetPhysicalDeviceProperties( availablePhysicalDevice, &properties );
        LOG_DEBUG( "  %s", properties.deviceName );

        if( properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU )
        {
            m_physicalDevice = availablePhysicalDevice;

            auto max_bit = std::bit_floor( properties.limits.framebufferColorSampleCounts );
            m_msaa = MSAA ? VkSampleCountFlagBits( max_bit ) : VK_SAMPLE_COUNT_1_BIT;
            break;
        }
    }

    getMemoryProperties();
    getQueueFamilies();
    getFormats();
}

/**************************************************************************************************/
VkPhysicalDeviceMemoryProperties PhysicalDevice::memoryProperties() const
{
    return m_memoryProperties;
}

/**************************************************************************************************/
VkSampleCountFlagBits PhysicalDevice::msaa() const
{
    return m_msaa;
}

/**************************************************************************************************/
uint32_t PhysicalDevice::queueFamilyGraphics() const
{
    return m_queueFamilyGraphics;
}

/**************************************************************************************************/
uint32_t PhysicalDevice::queueFamilyTransfer() const
{
    return m_queueFamilyTransfer;
}

/**************************************************************************************************/
uint32_t PhysicalDevice::queueFamilyCompute() const
{
    return m_queueFamilyCompute;
}

