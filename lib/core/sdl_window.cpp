#include "sdl_window.h"

/**************************************************************************************************/
WindowSdl2::WindowSdl2()
{}

/**************************************************************************************************/
WindowSdl2::~WindowSdl2()
{}

/**************************************************************************************************/
void WindowSdl2::create( const std::string &title )
{
    SDL_Init( SDL_INIT_VIDEO );

    SDL_Rect bounds{};
    SDL_GetDisplayBounds( 0, &bounds ); // primary screen

    m_window = SDL_CreateWindow(
                   title.c_str(),
                   SDL_WINDOWPOS_UNDEFINED,  SDL_WINDOWPOS_UNDEFINED,
                   1280, 720,
                   SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE );
    //SDL_WINDOW_KEYBOARD_GRABBED | SDL_WINDOW_RESIZABLE );

    uint32_t extension_count = 0;
    SDL_Vulkan_GetInstanceExtensions( m_window, &extension_count, nullptr );
    std::vector<const char *> instanceExtensions( extension_count );
    SDL_Vulkan_GetInstanceExtensions( m_window, &extension_count, instanceExtensions.data() );

    m_instanceExtensionNamesRequiredForSurface.clear();

    for( auto instanceExtension : instanceExtensions )
    {
        m_instanceExtensionNamesRequiredForSurface.push_back( std::string( instanceExtension ) );
    }
}


/**************************************************************************************************/
void WindowSdl2::destroy()
{
    SDL_DestroyWindow( m_window );
    SDL_Quit();
}

/**************************************************************************************************/
void WindowSdl2::createSurface( VkInstance instance,
                                VkSurfaceKHR *surface )
{
    SDL_Vulkan_CreateSurface( m_window, instance, surface );
}

/**************************************************************************************************/
bool  WindowSdl2::pollWindow()
{
    SDL_Event sdlEvent;

    while( SDL_PollEvent( &sdlEvent ) != 0 )
    {
        if( sdlEvent.type == SDL_QUIT )
        {
            return 0;
        }
    }

    return 1;
}

