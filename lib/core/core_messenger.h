#pragma once
#include <vulkan/vulkan.h>

class Messenger
{
public:
    Messenger();
    ~Messenger();

    void create( VkInstance instance );
    void destroy();

private:
    VkInstance m_instance = VK_NULL_HANDLE;
    VkDebugUtilsMessengerEXT m_messenger = VK_NULL_HANDLE;

    PFN_vkCreateDebugUtilsMessengerEXT vkCreateDebugUtilsMessengerEXT = nullptr;
    PFN_vkDestroyDebugUtilsMessengerEXT vkDestroyDebugUtilsMessengerEXT  = nullptr;;
};
