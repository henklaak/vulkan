#include "core_log.h"

#include <string>
#include <cstdarg>
#include <sys/time.h>

static int MIN_LOG_LEVEL = 20;

/**************************************************************************************************/
void VK::log( int level, const char *txt, ... )
{
    if( level < MIN_LOG_LEVEL )
    {
        return;
    }

    struct timeval tv;

    struct timezone tz;

    gettimeofday( &tv, &tz );

    double curtime = tv.tv_sec + tv.tv_usec / 1000000.0;

    time_t inttime = ( long long )curtime;

    struct tm *timeinfo = localtime( &inttime );

    char tst[256];

    snprintf( tst, 256, "%04d/%02d/%02d %02d:%02d:%02d.%06ld",
              1900 + timeinfo->tm_year,
              1 + timeinfo->tm_mon,
              timeinfo->tm_mday,
              timeinfo->tm_hour,
              timeinfo->tm_min,
              timeinfo->tm_sec,
              ( long )( 1000000 * ( curtime - inttime ) ) );

    va_list ap;

    va_start( ap, txt );

    char buf[256];

    vsnprintf( buf, 256, txt, ap );

    va_end( ap );

    FILE *fp = fopen( "log.txt", "at" );

    if( fp )
    {
        fprintf( fp, "%s %s\n", tst, buf );
        fclose( fp );
    }

    std::cout << buf << std::endl;
}

/**************************************************************************************************/
void VK::reset_log()
{
    FILE *fp = fopen( "log.txt", "wt" );

    if( fp )
    {
        fclose( fp );
    }

    LOG_DEBUG( "Restarted" );
}
