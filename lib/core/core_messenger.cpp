#include "core_messenger.h"
#include <sstream>
#include "core_log.h"

/**************************************************************************************************/
VKAPI_ATTR VkBool32 VKAPI_CALL debugUtilsMessengerCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT,
    const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData, void * )
{
    // Select prefix depending on flags passed to the callback
    std::string prefix( "" );

    if( messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT )
    {
        prefix = "VERBOSE: ";
    }
    else if( messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT )
    {
        prefix = "INFO: ";
    }
    else if( messageSeverity &
             VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT )
    {
        prefix = "WARNING: ";
    }
    else if( messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT )
    {
        prefix = "ERROR: ";
    }

    // Display message to default output (console/logcat)
    std::stringstream debugMessage;
    debugMessage << "-----------------------------------------" << std::endl
                 << prefix << std::endl
                 << pCallbackData->messageIdNumber << std::endl
                 << pCallbackData->pMessageIdName << std::endl
                 << pCallbackData->pMessage;

    if( messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT )
    {
        std::cerr << debugMessage.str() << "\n";
    }
    else
    {
        std::cout << debugMessage.str() << "\n";
    }

    fflush( stdout );

    return VK_FALSE;
}


/**************************************************************************************************/
Messenger::Messenger()
{
}

/**************************************************************************************************/
Messenger::~Messenger()
{
}

/**************************************************************************************************/
void Messenger::create( VkInstance instance )
{
    m_instance = instance;

    vkCreateDebugUtilsMessengerEXT = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(
                                         vkGetInstanceProcAddr( m_instance, "vkCreateDebugUtilsMessengerEXT" ) );

    vkDestroyDebugUtilsMessengerEXT = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(
                                          vkGetInstanceProcAddr( m_instance, "vkDestroyDebugUtilsMessengerEXT" ) );

    VkDebugUtilsMessengerCreateInfoEXT debugUtilsMessengerCI
    {
        .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
        .messageSeverity =
        //VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
        .messageType =
        VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT,
        .pfnUserCallback = debugUtilsMessengerCallback
    };

    VK_CHECK_RESULT( vkCreateDebugUtilsMessengerEXT( m_instance, &debugUtilsMessengerCI, nullptr, &m_messenger ) );
}

/**************************************************************************************************/
void Messenger::destroy()
{
    if( m_messenger )
    {

        vkDestroyDebugUtilsMessengerEXT( m_instance, m_messenger, nullptr );
        m_messenger = VK_NULL_HANDLE;
    }
}
